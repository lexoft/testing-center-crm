
import mongoose from 'mongoose';

const TestSpeciesSchema = new mongoose.Schema({
  name: { type: String, default: '' },
});

export default mongoose.model('TestSpecies', TestSpeciesSchema);

