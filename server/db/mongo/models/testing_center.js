
import mongoose from 'mongoose';

const TestingCenterSchema = new mongoose.Schema({
  full_name: { type: String, default: '' },
  short_name: { type: String, default: '' },
  adress: { type: String, default: '' },
  head: { type: String, default: '' },
  contact: { type: String, default: '' },
  position: { type: String, default: '' },
  threshold: {type: Number, default: 0 }
});


export default mongoose.model('TestingCenter', TestingCenterSchema);

