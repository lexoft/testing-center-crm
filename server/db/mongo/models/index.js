export default function loadModels() {
  require('./user');
  require('./testing_center.js');
  require('./test.js');
  require('./test_species.js');
}
