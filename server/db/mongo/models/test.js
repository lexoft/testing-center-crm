

import bcrypt from 'bcrypt-nodejs';
import mongoose from 'mongoose';
import autoIncrement from 'mongodb-autoincrement';

autoIncrement.setDefaults({
    collection: "tests",
    field: "_id",
    step: 1
});

const ScansSchema = new mongoose.Schema({ 
   _id: { type: mongoose.Schema.ObjectId, auto: true },
  name: String
});

const UpdateUserSchema = new mongoose.Schema({
    update_date: {type: Date, default: 0},
    name: { type: String, default: "" }
});

const TestSchema = new mongoose.Schema({
  _id: Number,
  updateUsers: [UpdateUserSchema],
  fio: {type: String, default: ""},
  surname: {type: String, default: ""},
  surname_trans: {type: String, default: ""},
  name: {type: String, default: ""},
  name_trans: {type: String, default: ""},
  birthday: {type: Date, default: 0},
  citizenship: {type: String, default: ""},
  document_type: {type: String, default: ""},
  series: {type: String, default: ""},
  number: {type: String, default: ""},
  date_of_issue: {type: Date, default: 0},
  issued_by: {type: String, default: ""},
  series_migration_cards: {type: String, default: ""},
  number_migration_cards: {type: String, default: ""},
  email: {type: String, default: ""},
  phone: {type: String, default: ""},
  scans: [ScansSchema],
  sex: { type: Boolean, default: false},
  registration_number:{type: Number, default: 0},
  search: { type: String, default: "" },
  registration_number_string: { type: String, default: "" },
  certification: {
    testing_center: {type: String, default: ""},
    test_date: { type: Date, default: 0},
    type_of_test: { type: String, default: "" },
    cost_test: { type: Number, default: 0 },
    discount: { type: Number, default: 0 },
    testing_result: { type: String, default: "" },
    certification_number: { type: String, default: "" },
    date_of_issue: { type: Date, default: 0 },
    comment: { type: String, default: "" },
    status: { type: String, default: "Новый"},
    training: { type: Boolean, default: false},
    result: {
      reading: {type: Number, default:0},
      letter: {type: Number, default:0},
      grammar: {type: Number, default:0},
      listening: {type: Number, default:0},
      speaking: {type: Number, default:0},
      history: {type: Number, default:0},
      law: {type: Number, default:0},
      all: {type: Number, default:0}
    }
  },
}, {_id: false});

TestSchema.plugin(autoIncrement.mongoosePlugin);
export default mongoose.model('Test', TestSchema);
