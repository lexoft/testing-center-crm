import _ from 'lodash';
import Test from '../models/test';
import TestingCenter from '../models/testing_center';
var ObjectId = require('mongodb').ObjectID;
import User from '../models/user';

export function testing_center(req, res, next){
  var arr=[];
  var maps=req.user.testing_center;
  maps.map(list => ( arr.push(ObjectId(list.id).toString())));
  if(req.body.testing_center!="all"){
    arr=[];
    arr.push(req.body.testing_center);
  }
    var first = new Date(+req.body.startDate);
    first.setMinutes(0);
    first.setHours(0);
    first.setMinutes(0);
    var last = new Date(+req.body.endDate);
    last.setDate(last.getDate());
    last.setHours(23);
    last.setMinutes(59);

    
Test.aggregate([
    {
      $match: {
        "certification.testing_center":{$in:arr},
        "certification.test_date":{$gte: first, $lte:last}
      }
    },
    { 
      "$group": {
        "_id": {
            "testing_center": "$certification.testing_center",
            "city": "$citizenship",            
        },
        gpa: {$sum: "$certification.result.all"},
        total:{$sum :1},
        issued:{$sum : { $cond: { if: {$or: [{$eq: ["$certification.status", 'Сертификат выдан']}, {$eq: ["$certification.status", 'Недействителен']}]}, then: 1, else: 0 } }},
        the_first:{$sum : { $cond: { if: {$and: [{$eq: ["$certification.discount", 0]}, {$or: [{$eq: ["$certification.status", 'Сертификат выдан']}, {$eq: ["$certification.status", 'Недействителен']}]}]},  then: 1, else: 0 } }},
        training:{$sum : { $cond: { if: {$eq: ["$certification.training", true]},  then: 1, else: 0} }},
        men:{$sum : { $cond: { if: {$eq: ["$sex", true]}, then: 1, else: 0} }},
        sum_cost:{$sum : "$certification.cost_test"},
        sum_discount:{$sum : "$certification.discount"},
        age: {
            $sum: { 
                  $let:{
                      vars:{
                          diff: { 
                             $divide: [{$subtract: [ new Date(), "$birthday" ] }, (365 * 24*60*60*1000)]  
                          }
                      },
                      in: {
                        $subtract: ["$$diff", {$mod: ["$$diff", 1]}]
                      }
                  }
              }
          },        
      }
    },
    { 
      $project : {
        testing_center : "$_id.testing_center", 
        city : "$_id.city",
        total: "$total",
        issued: "$issued",
        gpa: { $divide: [ {$multiply: [{$cond: [ { $eq: [ 500, 0 ] }, 0, {$divide: [ "$gpa", 500]}]}, 100]}, "$total"]},
        the_first: {$multiply: [{$cond: [ { $eq: [ "$issued", 0 ] }, 0, {$divide: [ "$the_first", "$issued"]}]}, 100]},
        training: {$multiply: [{$cond: [ { $eq: [ "$total", 0 ] }, 0, {$divide: [ "$training", "$total"]}]}, 100]},
        men: "$men",
        sum_cost: "$sum_cost",
        sum_discount: "$sum_discount",
        age: {$cond: [ { $eq: [ "$total", 0 ] }, 0, {$divide: [ "$age", "$total"]}]},
        _id : 0
      }
    },
    { $sort : { testing_center : -1 } }
    ]).exec( function(err, result) { 
            console.log(err);
            console.log(result);
              return res.status(200).send({
                                  "data":result
                                });  

          });
}

export function certificates(req, res, next) {
  var arr=[];
  var maps=req.user.testing_center;
  maps.map(list => ( arr.push(list.id)));
  var query = Test.find({field:{$ne:"_id"}, "certification.testing_center":{$in: arr}});

    var first = new Date(+req.body.startDate);
    first.setMinutes(0);
    first.setHours(0);
    first.setMinutes(0);
    var last = new Date(+req.body.endDate);
    last.setDate(last.getDate());
    last.setHours(23);
    last.setMinutes(59);
    query.count({"certification.date_of_issue":{$gte: first, $lte:last}} , function(err, count) {
    });  

  if(req.body.testing_center && req.body.testing_center!="all"){
    query.count({"certification.testing_center":req.body.testing_center} , function(err, count) {
    });    
  }
    query.count({"certification.status":{$in: ['Сертификат выдан','Недействителен']}} , function(err, count) {
    });
  query.sort({"certification.test_date": -1});
  query.exec('find', function(err, items) {
      /* items.forEach(function(elem, i) {
          // #TODOFIX Говнокод!!!!!!!!!!!!!!!!1
          User.find({_id: elem.updateUser}, function(err, user) {
              if (user && user.length > 0) {
                  items[i].updateUser = user[0].fio;
              } else {
                  items[i].updateUser ='Не указано'
              }
          });
      });
      */
      setTimeout(() => res.status(200).send({'data':items}), 1000);
    // return res.status(200).send({'data':items});
  });  
}

export function summary_table(req, res, next) {
  var arr=[];
  var maps=req.user.testing_center;
  maps.map(list => ( arr.push(list.id)));
  var query = Test.find({field:{$ne:"_id"}, "certification.testing_center":{$in: arr}});

    var first = new Date(+req.body.startDate);
    first.setMinutes(0);
    first.setHours(0);
    first.setMinutes(0);
    var last = new Date(+req.body.endDate);
    last.setDate(last.getDate());
    last.setHours(23);
    last.setMinutes(59);
    query.count({"certification.test_date":{$gte: first, $lte:last}} , function(err, count) {
    });  

  if(req.body.testing_center && req.body.testing_center!="all"){
    query.count({"certification.testing_center":req.body.testing_center} , function(err, count) {
    });    
  }
    query.count({"certification.status":{$in: ['Тестирование пройдено', 'Сертификат выдан','Недействителен']}} , function(err, count) {
    });
  query.sort({"certification.test_date": -1});
  query.exec('find', function(err, items) {    
    return res.status(200).send({'data':items});
  });  
}

export default {
  testing_center,
  certificates,
  summary_table
};
