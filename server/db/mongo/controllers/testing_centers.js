import _ from 'lodash';
import TestingCenter from '../models/testing_center';
import Test from '../models/test';
import User from '../models/user';


export function all(req, res) {
  TestingCenter.find({}).exec((err, testing_centers) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(testing_centers);
  });
}


export function add(req, res) {
  TestingCenter.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    return res.status(200).send({ message: 'Тестовый центер добавлен' });
  });
}


export function update(req, res, next) {
  var id = req.body.id;
  var full_name = req.body.full_name;
  var short_name = req.body.short_name;
  var head = req.body.head;
  var position = req.body.position;
  var adress = req.body.adress;
  var contact = req.body.contact;
  var threshold = req.body.threshold;
  
  TestingCenter.find({_id:req.body._id}).exec((err, tc) => {
    if (!tc) {
      console.log('Error in first query');
      return res.status(409).json({ message: 'Ошибка идентификации тестового центра!' });
    }  
    TestingCenter.update({ _id: id }, { $set: {'full_name':full_name, 'short_name':short_name, 'head':head, 'position':position, 'adress':adress, 'contact':contact, 'threshold':threshold} }, (err, result) => {
            User.update({"testing_center.id" : id}, {$set : {"testing_center.$.name" : short_name}},{multi: true}, (err, result3) => {                
            });      
            return res.status(200).json({
              message: 'Данные тестового центра успешно изменены'
            });           

    });
  });
}

export function remove(req, res) {

Test.count({"certification.testing_center":req.params.id, "certification.status":{$in:["Оплачен","Тестирование пройдено", "Сертификат выдан"]}}, (err, count) => {
    console.log( "Number of docs: ", count );
    if(count>0){
      return res.status(500).json({
        message: "Внимание! Удаляемый центр тестирования содержит записи с сертификатами запрещенные для удаления. Установите статусы сертификатов 'Недействителен' и попробуйте заново"
      });
    } else {
      TestingCenter.remove({"_id":req.params.id}, (err, result) => {
        if(result.result.n>0){       
          Test.remove({"certification.testing_center":req.params.id}, (err, result2) => {
            console.log(err);
            console.log(result2);
            User.update({}, { $pull: { testing_center: {id:  req.params.id} } },{multi: true}, (err, result3) => {
              console.log(err);
              console.log(result3);              
                  return res.status(200).json({
                    message: 'Тестовый центр успешно удален'
                  });                 
              });
            
            });
          }
        });
    }
});
}

export function testing_center_id(req, res) {
  TestingCenter.find({_id:req.params.id}).exec((err, tc) => {
    if (!tc) {
      console.log('Error in first query');
      return res.status(500).json({
          message: 'Такого тестового центра не существует'
        });
    }
        return res.status(200).json(tc[0]);
  });
}

export default {
  all,
  add,
  testing_center_id,
  update,
  remove
};
