import passport from 'passport';
import User from '../models/user';
import bcrypt from 'bcrypt-nodejs';
import mailer from '../../../init/mailer';
import MongoStore from '../session';


export function login(req, res, next) {
  passport.authenticate('local', (authErr, user, info) => {
    if (authErr) return next(authErr);
    if (!user) {
      return res.status(401).json({ message: 'Не верно указаны данные для входа!' });
    }
    if(user.active==false){ 
      return res.status(409).json({ message: 'Ваш аккаунт заблокирован администратором!' });
    }
    return req.logIn(user, (loginErr) => {
      if (loginErr) return res.status(401).json({ message: 'Не верно указаны данные для входа'});
      var profile = {
        _id:user._id,
        active:user.active,
        email:user.email,
        fio:user.fio,
        role:user.role,
        testing_center: user.testing_center,
      };
                return res.status(200).json({
                  message: 'Вы успешно вошли в систему',
                  profile: profile
                });

    });
  })(req, res, next);
}


export function logout(req, res) {
  req.logout();
  res.redirect('/');
}


function generateToken()
{
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    for( var i=0; i < 40; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}



export function password_reset(req, res, next) {
  var email = req.body.email;
  User.findOne({ email: req.body.email}, (findErr, existingUser) => {
    if (!existingUser) {
      return res.status(409).json({ message: 'Учетная запись не найдена!' });
    }
    var Token=generateToken();
    User.update({'email':email}, { $set: {'resetPasswordToken':Token,  resetPasswordExpires: new Date()} },(updErr) => {
      if (updErr) return res.status(409).json({ message: 'Ошибка!' });
      var to = email;
      var subj = 'Восстановление пароля';
      var text  = 'Для восстановление пароля перейдите по ссылке: https://test-center.bashedu.ru/password_reset/'+Token;
      mailer.init().then(function () {
        mailer.sendMail(to, subj, text)
          .then(function () {
            return res.status(200).json({
              message: 'Вам на почту было оправлено письмо с ссылкой для сброса пароля'
            });             
            console.log('Email was sent to ' + to);
          });
      });      
    });
  });
}

export function add(req, res, next) {
  const user = new User({
    email: req.body.email,
    password: req.body.password,
    fio: req.body.fio,
    role: req.body.role,
    testing_center: req.body.testing_center,
    active: true,
    resetPasswordToken: generateToken(),
    resetPasswordExpires: new Date()
  });
  User.findOne({ email: req.body.email }, (findErr, existingUser) => {
    if (existingUser) {
      return res.status(409).json({ message: 'Пользователь с такой почтой уже имеется в системе!' });
    }
    return user.save((saveErr) => {
      if (saveErr) return next(saveErr);
      var to = user.email;
      var subj = 'Регистрация на test-center';
      var text  = 'Для ввода нового пароля перейдите по ссылке: https://test-center.bashedu.ru/password_reset/'+user.resetPasswordToken;
      mailer.init().then(function () {
        mailer.sendMail(to, subj, text)
          .then(function () {
            return res.status(200).json({
              message: 'Пользователь добавлен.'
            });            
            console.log('Email was sent to ' + to);
          });
      });       
    });
  });
}

export function identificationResetToken(req, res, next) {
  var token = req.body.token;
  var d = new Date();
  d.setHours(d.getHours() - 12);
  User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: d } }, (findErr, existingToken) => {
    if (!existingToken) {
      return res.status(409).json({ message: 'Ошибка идентификации секретного ключа!' });
    }
        return res.status(200).json({
          message: 'Секретный ключ подтвержден.'
        });
  });
}

export function new_password(req, res, next) {
  var token = req.body.token;
  var password = req.body.password;
  var d = new Date();
  d.setHours(d.getHours() - 12); 

  var salt = bcrypt.genSaltSync(5);
  password = bcrypt.hashSync(password, salt, null);
    User.update({ resetPasswordToken: token, resetPasswordExpires: { $gt: d } }, { $set: {'password':password} }, (err, result) => {
        if (!result.nModified) {
          return res.status(409).json({ message: 'Ошибка идентификации секретного ключа!' });
        }
          return res.status(200).json({
              message: 'Пароль успешно изменен'
            }); 
    });
}

export function userid(req, res) {
  User.find({_id:req.params.id}).exec((err, users) => {
    if (users.length==0) {
      console.log('Error in first query');
      return res.status(500).json({
          message: 'Такого пользователя не существует'
        });
    }
        return res.status(200).json(users[0]);
  });
}




export function update(req, res, next) {
  var id = req.body.id;
  var email = req.body.email;
  var fio = req.body.fio;
  var role = req.body.role;
  var testing_center = req.body.testing_center;
  var password = req.body.password;
  if(password.length>0){
    var salt = bcrypt.genSaltSync(5);
    password = bcrypt.hashSync(password, salt, null);
  }
  User.findOne({ _id: id }, (findErr, existingId) => {
    if (!existingId) {
      return res.status(409).json({ message: 'Ошибка идентификации пользователя!' });
    }  
  User.findOne({ email: email }, (findErr, existingUser) => {
    if(existingUser){
        if (existingUser._id!=id) {
          return res.status(409).json({ message: 'Пользователь с такой почтой уже имеется в системе!' });
        }
    }
    User.update({ _id: id }, { $set: {'email':email, 'fio':fio, 'role':role, 'testing_center':testing_center} }, (err, result) => {
        if (err && (password.length==0)) {
          return res.status(409).json({ message: 'Ошибка идентификации пользователя!' });
        }
        if(password.length>0){
            User.update({ _id: id }, { $set: {'password':password } }, (err, result) => {
                  if (!result.nModified) {
                    return res.status(409).json({ message: 'Не удалось изменить пароль!' });
                  }
                    return res.status(200).json({
                        message: 'Пользовательские данные успешно изменены'
                      }); 
            });
          } else {
            return res.status(200).json({
              message: 'Пользовательские данные успешно изменены'
            });             
          }
    });
  });
});
}

export function remove(req, res, next){
  User.remove({_id: req.params.id}, (err, result) => {
    if(result.result.n==0){
      return res.status(409).json({ message: 'Ошибка идентификации пользователя!' });
    }
      return res.status(200).json({
        message: 'Пользователь успешно удален'
      });
  });
           
}

export function blockuserid(req, res, next) {

  User.update({_id:{ $in: req.body.block}}, {$set: {active:req.body.type}}, (err, result) => {
    if(result.nModified==0){
      return res.status(409).json({ message: 'Ошибка идентификации пользователя!' });
    }    

    if(result.nModified==1){
      if(req.body.type){
            return res.status(200).json({
              message: 'Пользователь успешно разблокирован'
            });
      } else {
          
            return res.status(200).json({
              message: 'Пользователь успешно заблокирован'
            });        
      }
    } else {
      return res.status(200).json({
        message: 'Пользователи успешно заблокированы'
      });      
    }
  });
           
}


export function all(req, res) {
  User.find({}).exec((err, users) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(users);
  });
}





export default {
  login,
  logout,
  all,
  add,
  password_reset,
  identificationResetToken,
  new_password,
  userid,
  update,
  remove,
  blockuserid
};
