import users from './users';
import testing_centers from './testing_centers';
import tests from './tests';
import test_species from './test_species';
import reports from './reports';
export { users, testing_centers , tests, test_species, reports};

export default {
  users,
  testing_centers,
  tests,
  test_species,
  reports
};
