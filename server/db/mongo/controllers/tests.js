import _ from 'lodash';
import Test from '../models/test';
import User from '../models/user';
import TestingCenter from '../models/testing_center';
import fs from 'fs';
import multiparty from 'multiparty';
var ObjectId = require('mongodb').ObjectID;
var pdf = require('html-pdf');

export function all(req, res, next) {
  //console.log(req.body);
    var arr=[];
    var maps=req.user.testing_center;
    maps.map(list => ( arr.push(ObjectId(list.id).toString())));
  let match = {
      "certification.testing_center": {$in:arr},
  };
  if(req.user.role === 0) {
      match['certification.status'] = "Сертификат выдан";
  }

  let match2 = {};
    if(req.body.selectCT){
        match2.testing_center = req.body.selectCT;
    }
    if(req.body.search) {
        var searchString = req.body.search;
        var search = [];
        var searchItems = searchString.split(" ");
        searchItems.forEach(function (searchItem) {
            search.push({
                search: {
                    $regex: searchItem, $options: 'i'
                }
            });
        });
        match2['$and'] = search;
        console.warn(search);
    }
    console.warn(match2);
    // match2.search = search;
    if(req.body.days=="today"){
        var first = new Date();
        first.setMinutes(0);
        first.setHours(0);
        first.setMinutes(0);
        var last = new Date();
        last.setMinutes(0);
        last.setHours(23);
        last.setMinutes(59);
        match2.test_date = {$gte: first, $lte:last};
    }
    if(req.body.days=="tomorrow"){
        var first = new Date();
        first.setDate(first.getDate()+1);
        first.setMinutes(0);
        first.setHours(0);
        first.setMinutes(0);
        var last = new Date();
        last.setMinutes(0);
        last.setDate(last.getDate()+1);
        last.setHours(23);
        last.setMinutes(59);
        match2.test_date = {$gte: first, $lte:last};
    }

    if(req.body.days=="week"){
        var first = new Date();
        first.setDate(first.getDate()-first.getDay()+1);
        first.setMinutes(0);
        first.setHours(0);
        first.setMinutes(0);
        var last = new Date();
        last.setDate(last.getDate()+(7-last.getDay()));
        last.setMinutes(0);
        last.setHours(23);
        last.setMinutes(59);
        match2.test_date = {$gte: first, $lte:last};
    }
    var matchSort = {
      _id: -1
    };
    if(req.body.sort !== "off"){
        var sort = req.body.sort.split('-');
        console.warn(sort);
        if(sort[0] && sort[1]) {
            matchSort = {};
            matchSort[sort[1]] = sort[0] === 'asc' ? 1 : -1;
        }
    }
    var query = Test.aggregate([
        {
            $match: match
        },
        {
            $project : {
                _id: "$_id",
                status: "$certification.status",
                fio: "$fio",
                registration_number: "$registration_number",
                certification_number: "$certification.certification_number",
                type_of_test: "$certification.type_of_test",
                test_date: "$certification.test_date",
                testing_center: "$certification.testing_center",
                citizenship: "$citizenship",
                cost_test: "$certification.cost_test",
                discount: "$certification.discount",
                search: {$concat: ["$fio", " ", "$registration_number_string", " ", "$certification.certification_number", " ", "$certification.type_of_test", " ", "$citizenship" ]},
            }
        },
        {
            $match: match2
        },
        {
            $sort: matchSort
        },
        { $group: {
            _id: null,
            list: {
                $push: {
                    _id: "$_id",
                    status: "$status",
                    fio: "$fio",
                    registration_number: "$registration_number",
                    certification_number: "$certification_number",
                    type_of_test: "$type_of_test",
                    test_date: "$test_date",
                    testing_center: "$testing_center",
                    citizenship: "$citizenship",
                    cost_test: "$cost_test",
                    discount: "$discount",
                }
            },
            count: { $sum: 1 },
            }
        },
        {
            $project: {
                list: { $slice: ["$list", ((req.body.page - 1) * req.body.pageSize), req.body.pageSize]},
                count: "$count"
            }
        }
    ]);

    query.exec(function(err, items) {
        console.warn(err);
        console.warn(items);
        Test.count().exec(function(err, totalTests) {
            Test.count({'certification.status': "Сертификат выдан"}).exec(function(err, totalIssued) {
                if (items.length === 0) {
                    items.push({
                        list: [],
                        count: 0
                    });
                }
                return res.status(200).send({
                    data: items[0].list,
                    totalTests,
                    totalIssued,
                    totalSize: items[0].count
                });
            });
        });
    });
}

export function getbyid(req, res){
  Test.findById(req.body.id, (err, testid) => {
    if(!testid){
              return res.status(400).send({
                                  message: 'Тестируемый не найден' 
                                });      
            }
    if(err) console.log(err);
    var send = JSON.parse(JSON.stringify(testid));
    var year, month, day;
    year=testid.birthday.getFullYear();
    month=testid.birthday.getMonth()+1;
    if(month<10) month='0'+month;
    day=testid.birthday.getDate();
    if(day<10) day='0'+day;
    send.birthday=year+'-'+month+'-'+day;
    if(testid.date_of_issue.getFullYear()==1970){
      send.date_of_issue="";
    } else{
      year=testid.date_of_issue.getFullYear();
      month=testid.date_of_issue.getMonth()+1;
      if(month<10) month='0'+month;
      day=testid.date_of_issue.getDate();
      if(day<10) day='0'+day;
      send.date_of_issue=year+'-'+month+'-'+day;            
    }
    if(testid.certification.date_of_issue.getFullYear()!=1970){
      year=testid.certification.date_of_issue.getFullYear();
      month=testid.certification.date_of_issue.getMonth()+1;
      if(month<10) month='0'+month;
      day=testid.certification.date_of_issue.getDate();
      if(day<10) day='0'+day;
      send.certification.date_of_issue=year+'-'+month+'-'+day;            
    } else {
      send.certification.date_of_issue="";
    }
    
    if(testid.certification.test_date.getFullYear()!=1970){
      year=testid.certification.test_date.getFullYear();
      month=testid.certification.test_date.getMonth()+1;
      if(month<10) month='0'+month;
      day=testid.certification.test_date.getDate();
      if(day<10) day='0'+day;
      send.certification.test_date=year+'-'+month+'-'+day;            
    } else {
      send.certification.test_date = "";
    }
    return res.status(200).json(send);
  });


}
export function uploadfile(req, res) {
  var form = new multiparty.Form();

  form.parse(req, (err, fields, files) => {

   
    var {path: tempPath, originalFilename} = files.file[0];    
    var a = originalFilename.split('.'); 
    var ext = a[a.length-1];
    var supportMimeTypes = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'odf', 'rtf', 'mpg', 'mpeg', 'm1v', 'mp2', 'mp3', 'mpa', 'mpe', 'm3u', 'avi', 'wav', 'mov', 'mp4', 'm4v', 'mp4v', 'm2ts', 'wmv', 'wma', 'wm', 'mkv', 'zip', '7z', 'rar'];
        if(supportMimeTypes.indexOf(ext) == -1) {
              return res.status(500).send({
                                  message: 'Для загрузки доступны видео, аудио и изображения' 
                                });
        }
          Test.findById(fields.id[0],function(err,gettest){
             gettest.scans.push({"name": originalFilename});
             gettest.save(function(err,story){
                 if(err){console.log(err);}
                 var copyToPath = "./scans/" + gettest.scans[gettest.scans.length-1]._id+'_'+originalFilename;
                  fs.readFile(tempPath, (err, data) => {
                    // make copy of image to new location
                    fs.writeFile(copyToPath, data, (err) => {
                      // delete temp image
                      fs.unlink(tempPath, () => {
                              return res.status(200).send({
                                  message: 'Документ успешно добавлен'
                                });

                      });

                  }); 
                });
             });
    }); 
  });
}
export function add(req, res) {
  var save = JSON.parse(JSON.stringify(req.body));
  delete save._id;
  save.fio=save.surname+" "+save.name;
  if(req.body.date_of_issue==""){
    save.date_of_issue=0;
  } else {
    save.date_of_issue = new Date(req.body.date_of_issue);
  }
  save.birthday = new Date(req.body.birthday);
  let updateUsers = { name: req.user.fio, update_date: new Date() };
  if(req.body._id=="new"){    
      Test.create({...save, updateUsers: [updateUsers]}, (err, result) => {
        if (err) {
          return res.status(400).send(err);
        }
        return res.status(200).send({ id: result._id,
                                      message: 'Тестируемый добавлен' 
                                    });
      });
  } else {
      Test.update({_id: req.body._id}, {$set: save} , (err, result) => {
          Test.update({_id: req.body._id}, {$push: {
              "updateUsers": updateUsers
          }}, (err, otv) => {
              console.log(otv);
              return res.status(200).send({ id: result._id,
                  message: 'Данные тестируемого изменены'
              });
          });
      });

  }
}
export function delete_test_scans(req, res, next) {
  Test.update({_id: req.body._id}, {$pull: { scans:{_id: new ObjectId(req.body.scans_id)}}} , (err, result) =>{
      var tempPath = './scans/'+req.body.scans_id+'_'+req.body.fname;
                        fs.unlink(tempPath, () => {
                        return res.status(200).json({
                          message: 'Файл успешно удален'
                        });
                      });      

  });   
}

export function certification_update(req, res) {

  var save = JSON.parse(JSON.stringify(req.body));
  console.log(save);
  save.result.all=save.result.reading+save.result.letter+save.result.grammar+save.result.listening+save.result.speaking+save.result.history+save.result.law;
  if(req.body.date_of_issue==""){
    save.date_of_issue=0;
  } else {
    save.date_of_issue = new Date( req.body.date_of_issue);
  }
  save.test_date = new Date(req.body.test_date);
  let updateUsers = { name: req.user.fio, update_date: new Date() };
    Test.update({_id: req.body.id},{$set: {
                                        'certification.status': save.status,
                                        'certification.training': save.training,
                                        'certification.test_date': save.test_date,
                                        'certification.type_of_test': save.type_of_test,
                                        'certification.cost_test': save.cost_test,
                                        'certification.discount': save.discount,
                                        'certification.testing_result': save.testing_result,
                                        'certification.certification_number': save.certification_number,
                                        'certification.date_of_issue': save.date_of_issue,
                                        'certification.comment': save.comment,
                                        'certification.result': save.result,
                                      }}, (err, result) => {
    if (err) {
      return res.status(400).send(err);
    }
    Test.update({_id: req.body.id}, {$push: {
        "updateUsers": updateUsers
    }}, (err, result) => {
        return res.status(200).send({
            message: 'Данные изменены'
        });
    });
  });
    Test.findById(req.body.id, (err, result)=>{
        console.log("one");
        console.log(result);
        if(result.registration_number==0 && result.certification.status=="Сертификат выдан"){
            Test.find({"registration_number":{$gt: 0}}).sort({"registration_number":1}).exec(function(err, list){
                console.log(err);
                console.log("two");
                console.log(list);
                try {
                    var miss = list[0].registration_number;
                } catch(e){
                    var miss=2;
                }
                console.log("three");
                console.log(miss);

                var new_number=0;
                if(miss>1) new_number=1; else {
                    new_number = list.reduce(function (prevVal, u) {
                        if ((u.registration_number - miss) > 1) {
                            console.log("ksks "+miss);
                            return miss;
                        } else miss = u.registration_number;
                        console.log("shag");
                        console.log(u.registration_number);
                        console.log(miss);
                    });
                    console.log("fetch");
                    console.log(new_number);
                    console.log("one1");
                    console.log(list.length-1);
                    console.log("one2");
                    console.log(list[list.length-1].registration_number);
                    //new_number=new_number.registration_number;
                    if (new_number) new_number =new_number + 1; else new_number = (list[list.length-1].registration_number) + 1;
                    if(list.length==1) new_number=(list[list.length-1].registration_number) + 1;
                }
                console.log("foo");
                console.log(new_number);

                Test.update({_id: req.body.id},{$set: {
                    'registration_number': new_number,
                    'registration_number_string': new_number
                }}, (err, result) => {
                    console.log(err);
                    console.log(result);
                });
            });

        }
    });
}


export function new_number(req, res) {
var promise = Test.findOne({field:{$ne:"_id"}}).sort('-_id').exec();
promise.then(function(item){
    return res.status(200).send({ 
                                  message: (item._id+1)
                                });
  })
  .catch(function(err){
      return res.status(200).send({ 
                                  message: ('1')
                                });    
  });
}

export function remove(req, res) {
  Test.remove({_id:req.params.id}, (err, result) => {
    if(result.result.n>0){
        return res.status(200).send({
                                      message: "Тестируемый успешно удален"
                                    });
      } else {
        return res.status(500).send({
                                      message: "Тестируемый не найден!"
                                    });
      }
  });
}

export function generate_pdf(req, res) {
Test.findById(req.body.id, (err, cert) => {
  console.log(cert);
  console.log(cert.certification.testing_center);
  TestingCenter.findById(new ObjectId(cert.certification.testing_center), (err, center) => {
    console.log(center);
    console.log(err);
    var html = fs.readFileSync('./server/templates/pdf/index.html', 'utf8');
    var options = { 
      format: 'A5', 
      orientation: "landscape",
      //width: '21cm',
      //height: '14.8cm',
    //"zoomFactor": "2",
    };
      var year=cert.certification.date_of_issue.getFullYear();
      var month=cert.certification.date_of_issue.getMonth()+1;
      if(month<10) month='0'+month;
      var day=cert.certification.date_of_issue.getDate();
      if(day<10) day='0'+day;
      var date_of_issue=day+'.'+month+'.'+year;
      var period=day+'.'+month+'.'+(year+2);
      var strID=new String(cert.registration_number);
    html = html.replace('{{id}}', ("00000000000" + strID).substring(strID.length));
    html = html.replace('{{date_of_issue}}', date_of_issue);
    html = html.replace('{{period}}', period);
    html = html.replace('{{name_ru}}', cert.surname+' '+cert.name);
    html = html.replace('{{name_en}}', cert.surname_trans+' '+cert.name_trans);
    html = html.replace('{{issuedBy}}', center.full_name);
    html = html.replace('{{head}}', center.head);
    html = html.replace('{{position}}', center.position);

    /*console.log(html);
    pdf.create(html, options).toStream(function(err, stream){
      stream.pipe(fs.createWriteStream('./'+(("00000000000" + strID).substring(strID.length))+'.pdf'));
    });  */  
    //res.set('Content-disposition', 'attachment; filename=1.pdf');//'+(("00000000000" + strID).substring(strID.length))+'
    //res.set('Content-type', 'application/pdf');
    //res.writeHead(200, {'Content-Type': 'application/pdf'});  
    //res.header('Content-Type': 'application/pdf' , 0 );
    pdf.create(html, options).toStream(function (err, stream) {
        if (err) console.log(err); 
        //return res.status(500).send({
        //                                message: 'Ошибка генерации файла'
        //                              });
        //res.writeHead(200, {'Content-Type': 'application/pdf'});  
        //res.header('Content-Type': 'application/pdf' , 0 );
        res.type('pdf');
        stream.pipe(res);
    });
   /* pdf.create(html, options).toFile('./1.pdf', function(err, res) {
      if (err) return console.log(err);
        return res.status(200).send({
                                      message: req.params.id
                                    });
    });     */ 
  });
});

        //return res.status(200).send({
         //                             message: req.params.id
          //                          });
}

export default {
  all,
  add,
  uploadfile,
  getbyid,
  delete_test_scans,
  certification_update,
  new_number,
  remove,
  generate_pdf
};
