import _ from 'lodash';
import TestSpecies from '../models/test_species';
var ObjectId = require('mongodb').ObjectID;



export function all(req, res) {
  TestSpecies.find({}).exec((err, species) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(species);
  });
}


export function add(req, res) {
  TestSpecies.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    return res.status(200).send({ message: 'Вид тестирования добавлен' });
  });
}


export function update(req, res, next) {
    TestSpecies.update({_id: req.body.id},{$set: {'name': req.body.name}}, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    return res.status(200).send({ 
      message: 'Название тестирования изменено' 
    });
  });
}

export function remove(req, res, next) {
      
      TestSpecies.remove({_id:{$in : req.body.keys }}, (err) => {
      if (err) {
        console.log(err);
        return res.status(400).send(err);
      }
      return res.status(200).send({ message: 'Виды тестирования успешно удалены' });        
      });
}

export function new_id(req, res, next) {
      return res.status(200).send({ message: new ObjectId() });
}

export default {
  all,
  add,
  update,
  remove,
  new_id
};
