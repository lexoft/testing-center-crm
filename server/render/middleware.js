import axios from 'axios';
import { createMemoryHistory, match } from 'react-router';
import createRoutes from '../../app/routes';
import configureStore from '../../app/store/configureStore';
import * as types from '../../app/types';
import { baseURL } from '../../config/app';
import pageRenderer from './pageRenderer';
import fetchDataForRoute from '../../app/utils/fetchDataForRoute';


axios.defaults.baseURL = baseURL;

export default function render(req, res) {
  let authenticated = req.isAuthenticated();
  if(authenticated && !req.user.active){ 
    authenticated=false;
    req.logout();
  }
  if(authenticated){
        var profile = {
          _id:req.user._id,
          active:req.user.active,
          email:req.user.email,
          fio:req.user.fio,
          role:req.user.role,
          testing_center: req.user.testing_center
        };
      }

  const history = createMemoryHistory();
  const store = configureStore({
    user: {
      authenticated,
      isWaiting: false,
      message: '',
      isLogin: true,
      profile
    },
    test: {
      loading: true
    }
  }, history);
  const routes = createRoutes(store);

 
  match({routes, location: req.url}, (err, redirect, props) => {
    if (err) {
      res.status(500).json(err);
    } else if (redirect) {
      res.redirect(302, redirect.pathname + redirect.search);
    } else if (props) {
      // This method waits for all render component
      // promises to resolve before returning to browser
      store.dispatch({ type: types.CREATE_REQUEST });
      fetchDataForRoute(props)
        .then(data => {
          store.dispatch({ type: types.REQUEST_SUCCESS, data });
          const html = pageRenderer(store, props);
          res.status(200).send(html);
   console.log(store.getState());
          
        })
        .catch(err => {
          console.error(err);
          res.status(500).json(err);
        });
    } else {
      res.sendStatus(404);
    }
  });
}
