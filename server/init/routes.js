/**
 * Routes for express app
 */
import passport from 'passport';
import unsupportedMessage from '../db/unsupportedMessage';
import { controllers, passport as passportConfig } from '../db';

const usersController = controllers && controllers.users;
const testingcenterController = controllers && controllers.testing_centers;
const testController = controllers && controllers.tests;
const testSpeciesController = controllers && controllers.test_species;
const reportsController = controllers && controllers.reports;
export default (app) => {
  function checkAuth(req, res, next){
    if(!req.isAuthenticated()){
      return res.status(500).send({'message':'Необходимо войти в систему'});
    } 
    next();
  }  

  function checkAdmin(req, res, next){
    if(req.user.role!=3){
      return res.status(500).send({'message':'У вас не хватает прав'});
    }
    next();
  }

  function onlyRead(req, res, next){
    if(req.user.role==0){
      return res.status(500).send({'message':'У вас не хватает прав'});
    }
    next();
  }  
  // user routes
  if (usersController) {
    app.post('/login', usersController.login);
    app.post('/logout', usersController.logout);

    app.get('/api/userslist', [checkAuth, checkAdmin, usersController.all]);

    app.get('/api/user/:id', [checkAuth, checkAdmin, usersController.userid]);
    app.post('/api/user', [checkAuth, checkAdmin, usersController.add]);
    app.put('/api/user', [checkAuth, checkAdmin, usersController.update]);
    app.delete('/api/user/:id', [checkAuth, checkAdmin, usersController.remove]);
    app.post('/api/blockuserid', [checkAuth, checkAdmin, usersController.blockuserid]);


    app.post('/api/password_reset', usersController.password_reset);
    app.post('/api/identificationResetToken', usersController.identificationResetToken);
    app.post('/api/new_password', usersController.new_password); 
  } else {
    console.warn(unsupportedMessage('users routes'));
  }



    // testing_centers routes
  if (testingcenterController) {
    app.get('/api/testing_centers', [checkAuth, testingcenterController.all]);

    app.post('/api/testing_center', [checkAuth, checkAdmin, testingcenterController.add]);
    app.get('/api/testing_center/:id', [checkAuth, testingcenterController.testing_center_id]);
    app.put('/api/testing_center', [checkAuth, checkAdmin, testingcenterController.update]);
    app.delete('/api/testing_center/:id', [checkAuth, checkAdmin, testingcenterController.remove]);
  } else {
    console.warn(unsupportedMessage('testing centers routes'));
  }

  if (testController) {
    app.post('/api/testlist', [checkAuth, testController.all]);
    app.post('/api/test_add', [checkAuth, onlyRead, testController.add]);   
    app.post('/api/test_new_number', [checkAuth, testController.new_number]);
    app.post('/api/uploadfile', [checkAuth, onlyRead, testController.uploadfile]);  
    app.post('/api/get_test_by_id', [checkAuth, testController.getbyid]);
    app.delete('/api/tested/:id', [checkAuth, onlyRead, testController.remove]);
    app.post('/api/delete_test_scans', [checkAuth, onlyRead, testController.delete_test_scans]);
    app.post('/api/certification_update', [checkAuth, onlyRead, testController.certification_update]);
    app.post('/api/generatepdf', [checkAuth, testController.generate_pdf]);
    app.post('/api/generatepdf', [checkAuth, testController.generate_pdf]);
  } else {
    console.warn(unsupportedMessage('tests routes'));
  }  


  if (testSpeciesController) {
    app.get('/api/species', [checkAuth, testSpeciesController.all]);
    app.post('/api/species', [checkAuth, checkAdmin, testSpeciesController.add]);
    app.put('/api/species', [checkAuth, checkAdmin, testSpeciesController.update]);  
    app.post('/api/species_del', [checkAuth, checkAdmin, testSpeciesController.remove]);
    app.get('/api/new_id_species', [checkAuth, checkAdmin, testSpeciesController.new_id]);
  } else {
    console.warn(unsupportedMessage('test species routes'));
  }    
  if (reportsController) {
    app.post('/api/reports/testing_center', [checkAuth, reportsController.testing_center]);
    app.post('/api/reports/certificates', [checkAuth, reportsController.certificates]);
    app.post('/api/reports/summary_table', [checkAuth, reportsController.summary_table]);
  } else {
    console.warn(unsupportedMessage('test species routes'));
  }  
};
