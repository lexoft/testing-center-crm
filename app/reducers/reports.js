import { combineReducers } from 'redux';
import * as types from '../types';


const list = (
  state = [],
  action
) => {
  switch (action.type) {
    case types.GET_REPORTS_SUCCESS:
      if (action.data) return action.data;
      return state;
    case types.GET_REPORTS:
    case "@@router/LOCATION_CHANGE":
      return [];       
    default:
      return state;
  }
};

const page = (
  state = 1,
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_PAGINATION:
      return action.data;
    case types.REQUEST_SUCCESS:
      return 1;      
    default:
      return state;
  }
};
const totalSize = (
  state = 1,
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_TOTAL_SIZE:
      return action.data;
    default:
      return state;
  }
};

const pageSize = (
  state = 10,
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_PAGE_SIZE:
      return action.data;
    default:
      return state;
  }
};

const search = (
  state = "",
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_SEARCH:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "";
    default:
      return state;
  }
};

const selectCT = (
  state = "",
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_SELECT:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "";      
    default:
      return state;
  }
};


const sort = (
  state = "off",
  action
) => {
  switch (action.type) {
    case types.UPDATE_REPORTS_SORT:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "off";      
    default:
      return state;
  }
};


const reportsReducer = combineReducers({
  pageSize,
  page,
  totalSize,
  list,
  selectCT,
  sort
});

export default reportsReducer;
