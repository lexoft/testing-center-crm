import * as types from '../types';


export default function message(state = {
  message: '',
  type: 'SUCCESS',
  print: 'message',
}, action = {}) {
  switch (action.type) {
    //зеленые алерты
    case types.LOGIN_SUCCESS_USER:    
    case types.SIGNUP_SUCCESS_USER:
    case types.ADD_USERSLIST_SUCCESS:
    case types.EDIT_USERID_SUCCESS:
    case types.BLOCK_USERID_SUCCESS:
    case types.DELETE_USERID_SUCCESS:
    case types.EDIT_TESTINGCENTERID_SUCCESS:
    case types.DELETE_TESTINGCENTERID_SUCCESS:
    case types.ADD_TEST_SPECIES_SUCCESS:
    case types.EDIT_TEST_SPECIES_SUCCESS:
    case types.DELETE_TEST_SPECIES_SUCCESS:
    case types.ADD_TEST_DATA_SUCCESS:
    case types.ADD_TEST_SCANS_SUCCESS:
    case types.DELETE_TEST_SCANS_SUCCESS:
    case types.UPDATE_TEST_CERTIFICATION_SUCCESS:
    case types.UPDATE_TEST_STATUS_SUCCESS:
    case types.DELETE_TEST_BY_ID_SUCCESS:
      return {...state, message: action.message, type: 'SUCCESS', print: 'alert'};

    //красные алерты
    case types.BLOCK_USERID_FAILURE:
    case types.DELETE_USERID_FAILURE:
    case types.GET_USERID_FAILURE:
    case types.GET_TESTINGCENTERID_FAILURE:
    case types.DELETE_TESTINGCENTERID_FAILURE:
    case types.ADD_TEST_SPECIES_ERROR:
    case types.EDIT_TEST_SPECIES_ERROR:
    case types.DELETE_TEST_SPECIES_ERROR:
    case types.ADD_TEST_DATA_FAILURE:
    case types.GET_TEST_BY_ID_ERROR:
    case types.ADD_TEST_SCANS_ERROR:
    case types.ADD_TEST_SCANS_ERROR:
    case types.DELETE_TEST_SCANS_ERROR:
    case types.UPDATE_TEST_CERTIFICATION_ERROR:
    case types.UPDATE_TEST_STATUS_ERROR:
    case types.DELETE_TEST_BY_ID_ERROR:
    case types.GENERATE_PDF_ERROR:
      return {...state, message: action.message, type: 'ERROR', print: 'alert'};

    //ошибки в виде текста
    case types.LOGIN_ERROR_USER:
    case types.PASSWORD_RESET_ERROR_USER:
    case types.ADD_USERSLIST_FAILURE:
    case types.EDIT_USERID_FAILURE:
    case types.EDIT_TESTINGCENTERID_FAILURE:
      return {...state, message: action.message, type: 'ERROR', print: 'message'};

    //зеленые сообщения
    case types.PASSWORD_RESET_SUCCESS_USER:
      return {...state, message: action.message, type: 'SUCCESS', print: 'message'};   

    //очистить сообщения   
    case types.REQUEST_SUCCESS:
    case types.DISMISS_MESSAGE:
    case types.ADD_USERSLIST:
    case types.EDIT_USERID:
    case types.GET_USERID:
    case types.GET_TESTINGCENTERID:
    case types.EDIT_TESTINGCENTERID:
      return {...state, message: '', type: 'SUCCESS'};
    default:
      return state;
  }
}
