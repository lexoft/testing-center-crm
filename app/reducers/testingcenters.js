import { combineReducers } from 'redux';
import * as types from '../types';

const addTestingCenterToggle = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.ADD_TESTINGCENTERS:
      return !state;
    case types.REQUEST_SUCCESS:
    case types.GET_TESTINGCENTERS_SUCCESS:
      return false;
    default:
      return state;
  }
};

const centerlist = (
  state = [],
  action
) => {
  switch (action.type) {  
    case types.GET_TESTINGCENTERS_SUCCESS:
      if (action.data) return action.data;
      return state;
    default:
      return state;
  }
};
const editTestingCenterToggle = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.EDIT_TESTINGCENTERID:
      return !state;
    case types.GET_TESTINGCENTERID:
    case types.REQUEST_SUCCESS:
      return false;
    default:
      return state;
  }
};
const testingCenterId = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.GET_TESTINGCENTERID_SUCCESS:
      if (action.data) return action.data;
      return state;
    default:
      return state;
  }
};
const testingCentersReducer = combineReducers({
  centerlist,
  addTestingCenterToggle,
  editTestingCenterToggle,
  testingCenterId
});

export default testingCentersReducer;
