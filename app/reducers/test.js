import { combineReducers } from 'redux';
import * as types from '../types';

const step = (
  state = 1,
  action
  ) => {
  switch(action.type){
    case types.TEST_CHANGENAV:
      return action.data;
    default:
      return state;
  }
};

const testid = (
  state = false,
  action
  ) => {
  switch(action.type){
    case types.GET_TEST_BY_ID_SUCCESS:
      return action.data;

    default:
      return state;
  }
};
const loading = (
  state = false,
  action
  ) => {
    switch(action.type){
      case types.GET_TEST_BY_ID:
        return true;
    case types.GET_TEST_BY_ID_SUCCESS:
    case types.GET_TEST_BY_ID_ERROR:
    case types.ADD_TEST_NEW_NUMBER:
      return false;        
      default:
        return state;
    }
};

const pdfloading = (
  state = false,
  action
  ) => {
    switch(action.type){
      case types.GENERATE_PDF:
        return true;
      case types.GENERATE_PDF_SUCCESS:
      case types.GENERATE_PDF__ERROR:
      case types.REQUEST_SUCCESS:
        return false;        
      default:
        return state;
    }
};

const testlist = (
  state = [],
  action
) => {
  switch (action.type) {
    case types.GET_TEST_LIST_SUCCESS:
      if (action.data) return action.data.data;
      return state;
    default:
      return state;
  }
};
const totalTests = (
  state = 0,
  action
) => {
  switch (action.type) {
    case types.GET_TEST_LIST_SUCCESS:
      if (action.data) return action.data.totalTests;
      return state;
    default:
      return state;
  }
};
const totalIssued = (
  state = 0,
  action
) => {
  switch (action.type) {
    case types.GET_TEST_LIST_SUCCESS:
      if (action.data) return action.data.totalIssued;
      return state;
    default:
      return state;
  }
};

const page = (
  state = 1,
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_PAGINATION:
      return action.data;
    case types.REQUEST_SUCCESS:
      return 1;      
    default:
      return state;
  }
};
const totalSize = (
  state = 1,
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_TOTAL_SIZE:
      return action.data;
    default:
      return state;
  }
};

const pageSize = (
  state = 10,
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_PAGE_SIZE:
      return action.data;
    default:
      return state;
  }
};

const search = (
  state = "",
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_SEARCH:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "";
    default:
      return state;
  }
};

const selectCT = (
  state = "",
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_SELECT:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "";      
    default:
      return state;
  }
};

const days = (
  state = "all",
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_DAYS:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "all";      
    default:
      return state;
  }
};

const sort = (
  state = "off",
  action
) => {
  switch (action.type) {
    case types.UPDATE_TEST_LIST_SORT:
      return action.data;
    case types.REQUEST_SUCCESS:
      return "off";      
    default:
      return state;
  }
};

const newNumber = (
  state = 100,
  action
) => {
  switch (action.type) {
    case types.ADD_TEST_NEW_NUMBER:
      return action.data;      
    default:
      return state;
  }
};

const testReducer = combineReducers({
  pageSize,
  page,
  totalSize,
  totalTests,
  totalIssued,
  loading,
  pdfloading,
  step,
  testid,
  testlist,
  search,
  selectCT,
  newNumber,
  days,
  sort
});

export default testReducer;
