import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import user from '../reducers/user';
import message from '../reducers/message';
import userslist from '../reducers/userslist';
import testspecies from '../reducers/testspecies';
import testingcenters from '../reducers/testingcenters'
import test from '../reducers/test'
import reports from '../reducers/reports'
import * as types from '../types';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case types.CREATE_REQUEST:
      return true;
    case types.REQUEST_SUCCESS:
    case types.REQUEST_FAILURE:
      return false;
    default:
      return state;
  }
};


const rootReducer = combineReducers({
  isFetching,
  user,
  testingcenters,
  userslist,
  testspecies,  
  message,
  routing,
  test,
  reports,
});

export default rootReducer;
