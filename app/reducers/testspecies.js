import { combineReducers } from 'redux';
import * as types from '../types';


const specieslist = (
  state = [],
  action
) => {
  switch (action.type) {
    case types.GET_TEST_SPECIES_SUCCESS:
      if (action.data) return action.data;
      return state;
    default:
      return state;
  }
};



const testSpeciesReducer = combineReducers({
  specieslist
});

export default testSpeciesReducer;
