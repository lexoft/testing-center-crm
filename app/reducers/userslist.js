import { combineReducers } from 'redux';
import * as types from '../types';

const addUserToggle = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.ADD_USERSLIST:
      return !state;
    case types.REQUEST_SUCCESS:
    case types.GET_USERSLIST_SUCCESS:
      return false;
    default:
      return state;
  }
};

const editUserToggle = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.EDIT_USERID:
      return !state;
    case types.GET_USERID:
    case types.REQUEST_SUCCESS:
    case types.GET_USERID_SUCCESS:
      return false;
    default:
      return state;
  }
};

const userslist = (
  state = [],
  action
) => {
  switch (action.type) {
    case types.GET_USERSLIST_SUCCESS:
      if (action.data) return action.data;
      return state;
    default:
      return state;
  }
};

const userId = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.GET_USERID_SUCCESS:
      if (action.data) return action.data;
      return state;
    default:
      return state;
  }
};


const usersListReducer = combineReducers({
  userslist,
  addUserToggle,
  userId,
  editUserToggle
});

export default usersListReducer;
