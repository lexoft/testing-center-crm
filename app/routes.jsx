import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import { fetchTesting_centersData } from './fetch-data';
import { App, Login, AdminApp, Users, PasswordReset, TestingCenters, UserId, InquiryPassword, TestingCenterId, TestedApp, Test, TestList, TestSpecies, ReportsApp, Reports, ReportsGenerate} from './pages';


export default (store) => {
  const requireAuth = (nextState, replace, callback) => {
    const { user: { authenticated }} = store.getState();
    if (!authenticated) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
    }
    callback();
  };

  const redirectAuth = (nextState, replace, callback) => {
    const { user: { authenticated }} = store.getState();
    if (authenticated) {
      replace({
        pathname: '/'
      });
    }
    callback();
  };

  const requireRoleAdmin = (nextState, replace, callback) => {
    const { user: { profile }} = store.getState();
    if (profile.role!=3) {
      replace({
        pathname: '/'
      });
    }
    callback();
  };

  return (
    <Route>
    <Route path="/" component={App}>
      <IndexRedirect to="tested"/>
      <Route path="login" component={Login} onEnter={redirectAuth} />
      <Route path="password_reset" component={InquiryPassword}  onEnter={redirectAuth}/>
      <Route path="password_reset/:token" component={PasswordReset}  onEnter={redirectAuth}/>

      <Route path="/reports" component={ReportsApp}   onEnter={requireAuth}>
        <IndexRoute component={Reports}/>
        <Route path=":type/:testing_center/:startDate/:endDate" component={ReportsGenerate}  onEnter={requireAuth}/>
      </Route>    

      <Route path="/tested" component={TestedApp}   onEnter={requireAuth}>
        <IndexRoute component={TestList} onEnter={requireAuth} />
        <Route path="test/:id" component={Test}  onEnter={requireAuth} />
      </Route>    

      <Route path="/admin" component={AdminApp}   onEnter={requireAuth, requireRoleAdmin}>
        <IndexRedirect to="users" />
        <Route path="users" component={Users}  onEnter={requireAuth}/>
        <Route path="testing_centers" component={TestingCenters} fetchData={fetchTesting_centersData}  onEnter={requireAuth}/>
        <Route path="testing_center/:id" component={TestingCenterId}  onEnter={requireAuth}/>
        <Route path="user/:id" component={UserId}  onEnter={requireAuth}/>
        <Route path="species" component={TestSpecies}  onEnter={requireAuth}/>
      </Route>
    </Route>
    </Route>
  );
};
