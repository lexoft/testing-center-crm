import React, { Component } from 'react';
import Page from '../pages/Page';
import InquiryPasswordContainer from '../containers/InquiryPassword';

class InquiryPassword extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Восстановление пароля';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <InquiryPasswordContainer {...this.props} />
      </Page>
    );
  }
}

export default InquiryPassword;
