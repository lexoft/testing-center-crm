import React, { Component } from 'react';
import Page from '../../pages/Page';
import CertificationContainer from '../../containers/Reports/Certification';

class ReportsCertification extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Реестр выданных сертификатов';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <CertificationContainer {...this.props} />
      </Page>
    );
  }
}

export default ReportsCertification;
