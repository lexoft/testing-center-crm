import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestingCenterContainer from '../../containers/Reports/TestingCenter';

class ReportsTestingCenter extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Аналитика по центрам тестирования';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestingCenterContainer {...this.props} />
      </Page>
    );
  }
}

export default ReportsTestingCenter;
