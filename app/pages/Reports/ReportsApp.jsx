import React from 'react';
import Page from '../../pages/Page';
import ReportsContainer from '../../containers/Reports/ReportsApp';
import { title, meta, link } from '../assets';

const ReportsApp = props => (
  <Page title={title} meta={meta} link={link}>
    <ReportsContainer {...props} />
  </Page>
);

export default ReportsApp;

