import React, { Component } from 'react';
import Page from '../../pages/Page';
import ReportsGenerateContainer from '../../containers/Reports/ReportsGenerate';

class ReportsGenerate extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Отчет';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <ReportsGenerateContainer {...this.props} />
      </Page>
    );
  }
}

export default ReportsGenerate;
