import React, { Component } from 'react';
import Page from '../pages/Page';
import ReportsContainer from '../containers/Reports';

class Reports extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Отчеты';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <ReportsContainer {...this.props} />
      </Page>
    );
  }
}

export default Reports;
