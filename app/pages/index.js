export { default as App } from 'pages/App';
export { default as Login } from 'pages/Login';
export { default as PasswordReset } from 'pages/PasswordReset';
export { default as InquiryPassword } from 'pages/InquiryPassword';


export { default as AdminApp } from 'pages/Admin/AdminApp';
export { default as Users } from 'pages/Admin/Users';
export { default as UserId } from 'pages/Admin/UserId';
export { default as TestingCenters } from 'pages/Admin/TestingCenters';
export { default as TestingCenterId } from 'pages/Admin/TestingCenterId';
export { default as TestSpecies } from 'pages/Admin/TestSpecies';

export { default as TestedApp } from 'pages/Tested/TestedApp';
export { default as Test } from 'pages/Tested/Test';
export { default as TestList } from 'pages/Tested/TestList';

export { default as ReportsApp } from 'pages/Reports/ReportsApp';
export { default as Reports } from 'pages/Reports/Reports';
export { default as ReportsGenerate } from 'pages/Reports/ReportsGenerate';
