import React, { Component } from 'react';
import Page from '../pages/Page';
import PasswordResetContainer from '../containers/PasswordReset';

class PasswordReset extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Восстановление пароля';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <PasswordResetContainer {...this.props} />
      </Page>
    );
  }
}

export default PasswordReset;
