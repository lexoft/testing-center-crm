import React from 'react';
import Page from '../../pages/Page';
import TestedContainer from '../../containers/Tested/TestedApp';
import { title, meta, link } from '../assets';

const TestedApp = props => (
  <Page title={title} meta={meta} link={link}>
    <TestedContainer {...props} />
  </Page>
);

export default TestedApp;

