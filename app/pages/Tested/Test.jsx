import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestContainer from '../../containers/Tested/Test';

class Test extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Тестируемый';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestContainer {...this.props} />
      </Page>
    );
  }
}

export default Test;
