import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestListContainer from '../../containers/Tested/TestList';

class TestList extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Список тестируемых';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestListContainer {...this.props} />
      </Page>
    );
  }
}

export default TestList;
