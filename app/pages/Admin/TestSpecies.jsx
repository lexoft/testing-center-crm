import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestSpeciesContainer from '../../containers/Admin/TestSpecies';

class TestSpecies extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Виды тестирования';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestSpeciesContainer {...this.props} />
      </Page>
    );
  }
}

export default TestSpecies;
