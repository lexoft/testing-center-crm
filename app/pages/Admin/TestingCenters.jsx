import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestingCentersContainer from '../../containers/Admin/TestingCenters';

class TestingCenters extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Центры тестирования';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestingCentersContainer {...this.props} />
      </Page>
    );
  }
}

export default TestingCenters;
