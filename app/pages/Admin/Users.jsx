import React, { Component } from 'react';
import Page from '../../pages/Page';
import UsersList from '../../containers/Admin/UsersList';

class Users extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Пользователи';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <UsersList {...this.props} />
      </Page>
    );
  }
}

export default Users;
