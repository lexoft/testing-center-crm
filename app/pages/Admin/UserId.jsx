import React, { Component } from 'react';
import Page from '../../pages/Page';
import UserIdContainer from '../../containers/Admin/UserId';

class UserId extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Пользователь';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <UserIdContainer {...this.props} />
      </Page>
    );
  }
}

export default UserId;
