import React, { Component } from 'react';
import Page from '../../pages/Page';
import TestingCenterIdContainer from '../../containers/Admin/TestingCenterId';

class TestingCenterId extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Тестовый центр';
  }

  pageMeta() {
    return [
      { name: 'description', content: '' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <TestingCenterIdContainer {...this.props} />
      </Page>
    );
  }
}

export default TestingCenterId;
