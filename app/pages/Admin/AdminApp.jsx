import React from 'react';
import Page from '../../pages/Page';
import AppContainer from '../../containers/Admin/AdminApp';
import { title, meta, link } from '../assets';
import '../../css/main.css';

const AdminApp = props => (
  <Page title={title} meta={meta} link={link}>
    <AppContainer {...props} />
  </Page>
);

export default AdminApp;

