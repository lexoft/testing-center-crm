import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { logOut } from '../actions/users';
import {Navbar, NavItem, MenuItem, NavDropdown, Nav} from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import  '../css/navhead';


const NavigationHead = ({ user, logOut, props }) => {
    return (
      <Navbar inverse collapseOnSelect>
      
        <Navbar.Header>
        
          <Navbar.Brand>
            ИРС
          </Navbar.Brand>
        
          <Navbar.Toggle />
        </Navbar.Header>
        
        <Navbar.Collapse>
        { user.authenticated ? (
          <Nav>          
          <LinkContainer to="/tested">
            <NavItem>Тестируемые</NavItem>
          </LinkContainer>
        
          { user.profile.role==3 ? (
            <LinkContainer to="/admin">
              <NavItem>Администрирование</NavItem>
            </LinkContainer>
            ) : ("") }
              { user.profile.role > 0 ? (
              <LinkContainer to="/reports">
                  <NavItem>Отчеты</NavItem>
              </LinkContainer>
                  ) : ("") }
          </Nav>
          ) : ("") }                      
                  { user.authenticated ? (
                    <Nav pullRight>  
                        <NavItem>{ user.profile.fio }</NavItem> 
                        <LinkContainer onClick={logOut} to="/logout">
                          <NavItem>Выход</NavItem>
                        </LinkContainer>
                    </Nav>
                      ) : (
                      <Nav pullRight>  
                        <LinkContainer  to="/login">
                          <NavItem>Войти</NavItem>
                        </LinkContainer>
                      </Nav>
                  )}                
        </Navbar.Collapse>
      </Navbar>
    );
};
NavigationHead.propTypes = {
  user: PropTypes.object,
  logOut: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    props: state,
    user: state.user
  };
}

export default connect(mapStateToProps, { logOut })(NavigationHead);
