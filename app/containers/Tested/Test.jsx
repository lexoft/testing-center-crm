import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Col, Form, ControlLabel, FormControl, FieldGroup, FormGroup, Button, ButtonGroup} from 'react-bootstrap';
import ReactDOM from 'react-dom';
import Message from '../Message'; 
import Navigation from './Navigation';
import Data from '../../components/Test/Data';
import Scans from '../../components/Test/Scans';
import Certification from '../../components/Test/Certification';
import { deleteTestId, addData, uploadFile, getTestById,getNewNumber, NavStep, addTestScansSuccess, addTestScansError, deleteTestScans, updateCertification, generatePDF } from '../../actions/test';
import { getTestSpecies } from '../../actions/testspecies';
import { getTestingCenterId } from '../../actions/testingcenters';

class Test extends Component {
	constructor(props) {
	    super(props);
  	}
componentDidMount(){
  const {id, getTestById, NavStep, getTestSpecies, getNewNumber} = this.props;
  NavStep(1);
  getTestSpecies();
  if(id!="new"){
    getTestById({'id':id});
  } else {
    getNewNumber();
  }
}

    render() {
    const {test: {testid, step, loading, newNumber}, id, addData, getTestById,  addTestScansSuccess, addTestScansError, updateCertification, deleteTestScans, user, deleteTestId, generatePDF} = this.props;
    return (
    <div>
      <Col md={12}><Col md={6}><h2 style={{display:"inline-block"}}>{ id=="new" ? "Новое тестирование №"+newNumber: "Тестирование №"+testid._id}</h2></Col>{ (id!="new" && testid.registration_number>0) ? <Col md={5} className="pull-right"><h2 style={{display:"inline-block"}}>Рег. номер: {("00000000000" + testid.registration_number).substring(testid.registration_number.length)}</h2></Col>: ""}</Col>
      {user.profile.role != "0" &&
      <Col md={1} className="col-nav">
        <Navigation {...this.props}/>
      </Col>
      }
      <Col md={9}>
      { loading ? "Загрузка..." :(
      <div>
      <Message/>
                { step==1 || user.profile.role === 0 ? <Data {...this.props}/> : "" }
                { step==3 || user.profile.role === 0 ? <Certification {...this.props}/> : "" }
                { step==2 && <Scans {...this.props}/> }
      </div>
      ) }
      </Col>
    </div>
      );
  }
}
Test.propTypes = {
  addData: PropTypes.func.isRequired,
  getTestById: PropTypes.func.isRequired,
  NavStep: PropTypes.func.isRequired,
  addTestScansSuccess: PropTypes.func.isRequired,
  addTestScansError: PropTypes.func.isRequired,
  deleteTestScans: PropTypes.func.isRequired,
  updateCertification: PropTypes.func.isRequired,
  getTestSpecies: PropTypes.func.isRequired,
  getNewNumber: PropTypes.func.isRequired,
  deleteTestId: PropTypes.func.isRequired,
  getTestingCenterId: PropTypes.func.isRequired,
  generatePDF: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    id: ownProps.params.id,
    test: state.test,
    user: state.user,
  };
}

export default connect(mapStateToProps, {deleteTestId, getTestSpecies, getNewNumber, addData, uploadFile, getTestById, updateCertification, NavStep, deleteTestScans, addTestScansSuccess, addTestScansError, getTestingCenterId, generatePDF})(Test);
