import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { NavStep } from '../../actions/test';
import { Nav, Carousel } from 'react-bootstrap';
import NavItem  from 'react-bootstrap/lib/NavItem';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import Navbar from 'react-bootstrap/lib/Navbar';
import '../../css/icon_add_tested.css';


const Navigation = ({id, test, NavStep, deleteTestId, user}) => {
  function changeSelect(data){
    if(data!=4){  
      NavStep(data);
    } else {
      alertify
        .okBtn("Удалить")
        .cancelBtn("Отмена")
        .confirm("Вы действительно хотите удалить тестируемого?", function (ev) {
            deleteTestId({"_id": id});
        });
    }
  }
  function dateFormatter(data){
      var date = new Date(data);
      var day=date.getDate();
      var month=date.getMonth()+1;
      var year=date.getFullYear();
      if(day<10) day='0'+day;
      if(month<10) month='0'+month;
      if (year === 1970) {
          return '';
      }
      return (day+'.'+month+'.'+year + ' ' + date.getHours() + ':' + (date.getMinutes()<10 ? '0' + date.getMinutes() : date.getMinutes()));
  }
  var ok1 = false;
  var ok2 = false;
  var ok3 = false;
  if(id == "new"){
      var disabled=true;
  } else {
    if(test.testid!=false){
        if(test.testid.name) ok1=true;        
        if(test.testid.certification.test_date!="") ok3=true;
        if(test['testid']['scans'].length!=0) ok2=true;
      }
  }
  return (
    <div>
    <Nav bsStyle="pills" stacked activeKey={test.step} onSelect={ changeSelect }>
        <NavItem eventKey={1}>Данные тестируемого{ ok1 ? <span className="glyphicon glyphicon-ok icon"></span> : ""}</NavItem>
        <NavItem eventKey={2} disabled={disabled}>Сканы документов{ ok2 ? <span className="glyphicon glyphicon-ok icon"></span> : ""}</NavItem>
        <NavItem eventKey={3} disabled={disabled}>Сертификации{ ok3 ? <span className="glyphicon glyphicon-ok icon"></span> : ""}</NavItem>
        { user.profile.role!="0" ?
        <NavItem eventKey={4} disabled={disabled}>Удалить{ ok1 ? <span className="glyphicon glyphicon-remove remove"></span> : ""}</NavItem>
        : "" }
    </Nav>
        <div style={{ paddingLeft: '15px' }}>
            <h4>Редактировал:</h4>
            {test.testid.updateUsers && test.testid.updateUsers.length > 0 ?
                <Carousel indicators={false} wrap={false} interval={100000}>
                    {test.testid.updateUsers.slice(0).reverse().map((val) => <Carousel.Item><center><p>{val.name}</p>{dateFormatter(val.update_date)}</center></Carousel.Item>)}
                </Carousel>
                : 'Не указано'}
        </div>
    </div>
  );
};

Navigation.propTypes = {
  id: PropTypes.string.isRequired,
  test: PropTypes.object,
  NavStep: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    test: state.test
  };
}

export default connect(mapStateToProps, { NavStep })(Navigation);
