import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getTestList, updatePage, updatePageSize, updateSearch, updateSelect, updateDays, updateSort} from '../../actions/test';
import TestListTable from '../../components/Test/TestListTable';
import Button from 'react-bootstrap/lib/Button';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';


class TestList extends Component {
	constructor(props) {
	    super(props);
      this.onPageChange = this.onPageChange.bind(this);
      this.onSizePerPageList = this.onSizePerPageList.bind(this);
      this.inputChange = this.inputChange.bind(this);
      this.selectChange = this.selectChange.bind(this);
      this.updateDaysChange = this.updateDaysChange.bind(this);
      this.onSortChange = this.onSortChange.bind(this);
  	}

  componentDidMount() {
  	const { getTestList , test:{pageSize, page}} = this.props;
     getTestList(pageSize, page, "", "", "all", "off");
  }
  onPageChange(page, sizePerPage) {
    const {updatePage, getTestList, test:{search, selectCT, days, sort}} = this.props;
    updatePage(page);
    getTestList(sizePerPage, page, search, selectCT, days, sort);
  }

   onSizePerPageList(sizePerPage) {
    const {updatePageSize, updatePage, getTestList, test: {page, pageSize}} = this.props;
    updatePageSize(sizePerPage);
  }
  selectChange(data){
    const {updatePage, getTestList, test: {page, pageSize, search, days, sort}, updateSelect} = this.props;
    updateSelect(data);
    updatePage(1);
    getTestList(pageSize, 1, search, data, days, sort);    
  }
  inputChange(e){
    e.preventDefault();
    const {updatePage, getTestList, test: {page, pageSize, selectCT, days, sort}, updateSearch} = this.props;
    updateSearch(e.target.value);
    updatePage(1);
    getTestList(pageSize, 1, e.target.value, selectCT, days, sort);
    //console.log(e.target.value);
  }
  updateDaysChange(e){
    e.preventDefault();
    const {updateDays, test: {page, pageSize, selectCT, search, sort}, getTestList} = this.props;
    updateDays(e.target.value);
    getTestList(pageSize, 1, search, selectCT, e.target.value, sort);
    //console.log(e.target.value);
  }  
  onSortChange(sortName, sortOrder){
    const {updateSort, test: {page, pageSize, selectCT, days, search}, getTestList} = this.props;
    getTestList(pageSize, 1, search, selectCT, days, sortOrder + '-' + sortName);
    updateSort(sortOrder + '-' + sortName);
    if (sortOrder === 'asc') {
      return 1;
    } else {
      return 0;
    }
  }
  render() {
  	const {test: { testlist, totalSize, page, pageSize, days, selectCT, totalTests, totalIssued },user,  user:{ profile }} = this.props;
      return (  
        <div>
          <TestListTable
            testlist={testlist}
            testing_center={profile.testing_center}
            onPageChange={ this.onPageChange } 
            totalSize={totalSize}
            page={page}
            pageSize={pageSize}
            onSizePerPageList={ this.onSizePerPageList }
            selectChange={this.selectChange}
            inputChange={this.inputChange}
            updateDaysChange={this.updateDaysChange}
            days={days}
            onSortChange={this.onSortChange}
            user={user}
            selectCT={selectCT}
            totalTests={totalTests}
            totalIssued={totalIssued}
          />
        </div>
      );
  }
}
TestList.propTypes = {
  getTestList: PropTypes.func.isRequired,
  updatePage: PropTypes.func.isRequired,
  updatePageSize: PropTypes.func.isRequired,
  updateSearch: PropTypes.func.isRequired,
  updateSelect: PropTypes.func.isRequired,
  updateDays: PropTypes.func.isRequired,
  updateSort: PropTypes.func.isRequired,
};


function mapStateToProps(state) {
  return {
    test: state.test,
    user: state.user,
  };
}

export default connect(mapStateToProps, { getTestList, updatePage, updatePageSize, updateSearch, updateSelect, updateDays, updateSort})(TestList);
