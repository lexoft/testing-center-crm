import React, { PropTypes } from 'react';
import Message from '../../containers/Message';
import NavigationHead from '../../containers/NavigationHead';
import Col from 'react-bootstrap/lib/Col'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
const TestedApp = ({children}) => {
  return (
    <Row className="show-grid">
	        {children}
    </Row>
  );
};

TestedApp.propTypes = {
  children: PropTypes.object
};

export default TestedApp;
