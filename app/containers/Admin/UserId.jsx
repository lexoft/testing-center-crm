import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getUserId, editUserMode, editSaveUserId, deleteUserId, blockUserId} from '../../actions/userslist';
import { getTestingCenters } from '../../actions/testingcenters';
import {Col, Form, ControlLabel, FormControl, FieldGroup, FormGroup, Button, ButtonGroup} from 'react-bootstrap';
import ReactDOM from 'react-dom';
import Message from '../Message';
import Select from 'react-select';
import '../../css/react-select.css';


class UserId extends Component {
	constructor(props) {
	    super(props);
	    this.handleOnSubmit = this.handleOnSubmit.bind(this);
      this.deleteUserIdHandle = this.deleteUserIdHandle.bind(this);
      this.blockUserIdHandle = this.blockUserIdHandle.bind(this);
      this.updateRoleValue = this.updateRoleValue.bind(this);
      this.updateCenterListValue = this.updateCenterListValue.bind(this);      
      this.state = {
        selectCenter:'',
        role:0
      };
  	}

  handleOnSubmit(event) {
    event.preventDefault();
    const {testingcenters:{centerlist}}=this.props;
    var new_centerlist={};
    var testing_center=[];
    centerlist.map(list => ( new_centerlist[list._id]=list.short_name));
    var centersSelect=this.state.selectCenter.split(',');
    if(centersSelect[0]=="") centersSelect=[];
    testing_center = centersSelect.map(function(option){
        return ({'id': option, 'name':new_centerlist[option]});
    });
    const role = this.state.role;
    const fio = ReactDOM.findDOMNode(this.refs.fio).value;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    const password = ReactDOM.findDOMNode(this.refs.password).value;
    const { editSaveUserId,id } = this.props;
    editSaveUserId({id, fio, email, password, testing_center, role});
  }

  componentDidMount() {
  	const {userslist: {userId}, getUserId, getTestingCenters ,id} = this.props;
     getUserId(id);
     getTestingCenters();

  }
  componentWillReceiveProps(){
      const {userslist: {userId}} = this.props;
      this.setState({
        role:userId.role,
      });
      var strCenter='';
      if(userId["testing_center"])  userId["testing_center"].map(list => { strCenter+=','+list.id });
      if(strCenter[0]==',') strCenter=strCenter.substring(1, strCenter.length);
      this.setState({
        selectCenter: strCenter,
      })
  }
  deleteUserIdHandle(){
    const {userslist: {userId}, deleteUserId} = this.props;
    alertify
      .okBtn("Удалить")
      .cancelBtn("Отмена")
      .confirm("Удалить пользователя "+userId.fio+"?", function (ev) {
          deleteUserId({"id": userId._id});
      });    
  }
  blockUserIdHandle(){
    const {userslist: {userId}, blockUserId} = this.props;
    alertify
      .okBtn(userId.active ? "Заблокировать" : "Разблокировать")
      .cancelBtn("Отмена")
      .confirm((userId.active ? "Заблокировать" : "Разблокировать" )+" пользователя "+userId.fio+"?", function (ev) {
        blockUserId({"block": [userId._id], "type":!userId.active});
      });     
  }
  updateRoleValue (newValue) {
    this.setState({
      role: newValue
    });
  }
  updateCenterListValue (newValue) {
    this.setState({
      selectCenter: newValue
    });
  }  
  render() {
      const {userslist: {userId, userId: {testing_center}, editUserToggle}, testingcenters:{centerlist}, editUserMode} = this.props;  
      
      return (
      <Form horizontal onSubmit={this.handleOnSubmit}>
      <h1>Пользователь {userId.fio}</h1>
      <ButtonGroup>
        <Button className="btn-primary" onClick={ editUserMode }>Редактировать</Button>
        <Button className="btn-primary" onClick={ this.deleteUserIdHandle }>Удалить</Button>
        <Button className="btn-primary" onClick={ this.blockUserIdHandle }>{ userId.active ? "Заблокировать" : "Разблокировать"}</Button>
      </ButtonGroup>
          <FormGroup controlId="formHorizontalFio">
          <Message/>
            <Col componentClass={ControlLabel} sm={2}>ФИО:</Col>
            <Col sm={3}>
            { editUserToggle ? <FormControl type="text"
                placeholder="Enter text"
                ref="fio"
                defaultValue={userId.fio}
                 /> : <h4>{userId.fio}</h4>}
    
            </Col>
          </FormGroup>

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Email:</Col>
              <Col sm={3}>
               { editUserToggle ?
              <FormControl type="mail"
                placeholder="Enter text"
                ref="email"
                defaultValue={userId.email}
                 />
                : <h4>{userId.email}</h4>}
              </Col>   
          </FormGroup>
            { editUserToggle ?
              <FormGroup controlId="formControlsPassword"> 
                <Col componentClass={ControlLabel} sm={2}>Новый пароль:</Col>
                <Col sm={3}>
                  
                  <FormControl type="password"
                    placeholder="Оставить старый пароль"
                    ref="password"
                     />
                </Col>  
              </FormGroup>
            : ""}
          <FormGroup controlId="formControlscenter"> 
            <Col componentClass={ControlLabel} sm={2}>Центры тестирования:</Col>
            <Col sm={3}>
            { editUserToggle ?
              <Select multi ref="centerlist"
              clearAllText="Очистить"
                options={centerlist} 
                labelKey="short_name" valueKey="_id"
                value={this.state.selectCenter}
                onChange={this.updateCenterListValue}
                placeholder=''
              simpleValue searchable={false} clearable={true}
              /> 
            : userId["testing_center"] ? userId["testing_center"].map(list => <h4>{ list.name }</h4>) : ""}
            </Col>  
          </FormGroup>   
          <FormGroup controlId="formControlsRole"> 
            <Col componentClass={ControlLabel} sm={2}>Роль:</Col>
            <Col sm={3}>
            { editUserToggle ?
              <Select ref="role" 
                options={[
                    {value: 0, label: 'Наблюдатель'},
                    {value: 1, label: 'Менеджер'},
                    {value: 2, label: 'Руководитель'},
                    {value: 3, label: 'Администратор системы'}
                  ]} 
                value={this.state.role}
                onChange={this.updateRoleValue}
              simpleValue searchable={false} clearable={false}
              /> 
            : userId.role == 0 ? <h4>Наблюдатель</h4>: (userId.role == 1 ? <h4>Менеджер</h4>: (userId.role == 2 ? <h4>Руководитель</h4>:(userId.role == 3 ? <h4>Администратор</h4>:"")))}
            </Col>  
          </FormGroup> 
          { editUserToggle ?
          <FormGroup>
            <Col smOffset={2} sm={3}>                                 
            <Button className="btn-primary" type="submit"
            >
             Сохранить
            </Button>            
            </Col>
          </FormGroup>
          : ""}
      </Form>
      );
  }
}
UserId.propTypes = {
  userslist: PropTypes.object,
  getUserId: PropTypes.func.isRequired,
  editUserMode: PropTypes.func.isRequired,
  getTestingCenters: PropTypes.func.isRequired,
  editSaveUserId: PropTypes.func.isRequired,
  deleteUserIdHandle: PropTypes.func.isRequired,
  deleteUserId: PropTypes.func.isRequired,
  blockUserId: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    userslist: state.userslist,
    testingcenters: state.testingcenters,
    id: ownProps.params.id
  };
}

// Read more about where to place `connect` here:
// https://github.com/rackt/react-redux/issues/75#issuecomment-135436563
export default connect(mapStateToProps, { getUserId, getTestingCenters, editUserMode, editSaveUserId, deleteUserId, blockUserId})(UserId);
