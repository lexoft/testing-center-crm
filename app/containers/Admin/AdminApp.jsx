import React, { PropTypes } from 'react';
import Message from '../../containers/Message';
import NavigationHead from '../../containers/NavigationHead';
import NavigationMenu from './Navigation'
import Col from 'react-bootstrap/lib/Col'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
const AdminApp = ({children}) => {
  return (
    <Row className="show-grid">
	    <Col md={2}>
	      <NavigationMenu/>
	    </Col>
	    <Col md={10}>
	        {children}
	    </Col>
    </Row>
  );
};

AdminApp.propTypes = {
  children: PropTypes.object
};

export default AdminApp;
