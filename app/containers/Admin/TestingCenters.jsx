import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getTestingCenters, addTestingCenter, addTestingCenterMode} from '../../actions/testingcenters';
import TestingCentersTable from '../../components/Admin/TestingCentersTable';
import TestingCentersAdd from '../../components/Admin/TestingCentersAdd';
import Button from 'react-bootstrap/lib/Button';


class TestingCenters extends Component {
	constructor(props) {
	    super(props);
	    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  	}
  handleOnSubmit(event) {
    event.preventDefault();
    const { addTestingCenter }= this.props;
    addTestingCenter();
  }

  componentDidMount() {
  	const { getTestingCenters } = this.props;
     getTestingCenters();
  }
  render() {
  	const {testingcenters: { addTestingCenterToggle },testingcenters: { centerlist }, addTestingCenterMode , addTestingCenter} = this.props;
    if(addTestingCenterToggle){
      return (
        <div >
        <Button
          bsStyle="primary"
          onClick={ addTestingCenterMode }>
          { addTestingCenterToggle? "Список центров тестирования" : "Добавить" }
        </Button>
            <TestingCentersAdd 
            onEntrySave={ addTestingCenter }
            />
        </div>
      );
    }
      return (
        <div >

          <TestingCentersTable
            centerlist={centerlist}
            addTestingCenterMode={ addTestingCenterMode }
             />
        </div>
      );
  }
}
TestingCenters.propTypes = {
  testingcenters: PropTypes.object,
  getTestingCenters: PropTypes.func.isRequired,
  addTestingCenter: PropTypes.func.isRequired,
  addTestingCenterMode: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    testingcenters: state.testingcenters
  };
}

export default connect(mapStateToProps, {getTestingCenters, addTestingCenter, addTestingCenterMode})(TestingCenters);
