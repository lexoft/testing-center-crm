import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { logOut } from '../../actions/users';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem  from 'react-bootstrap/lib/NavItem';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import Navbar from 'react-bootstrap/lib/Navbar';

const Navigation = ({ user, logOut, props }) => {
    return (
    <div>
    <Nav bsStyle="pills" stacked>
      <LinkContainer to="/admin/users">

        <NavItem>Пользователи</NavItem>
      </LinkContainer>
      <LinkContainer to="/admin/testing_centers">
        <NavItem>Центры тестирования</NavItem>
      </LinkContainer>           
      <LinkContainer to="/admin/species">
        <NavItem>Виды тестирования</NavItem>
      </LinkContainer>      
    </Nav>
    </div>
    );
};

Navigation.propTypes = {
  user: PropTypes.object,
  logOut: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    props: state,
    user: state.user
  };
}

export default connect(mapStateToProps, { logOut })(Navigation);
