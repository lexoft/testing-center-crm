import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getTestingCenterId, editTestingCenterMode, editSaveTestingCenterId, deleteTestingCenterId} from '../../actions/testingcenters';
import {Col, Form, ControlLabel, FormControl, FieldGroup, FormGroup, Button, ButtonGroup} from 'react-bootstrap';
import ReactDOM from 'react-dom';
import Message from '../Message';

class TestingCenterId extends Component {
	constructor(props) {
	    super(props);
	    this.handleOnSubmit = this.handleOnSubmit.bind(this);
      this.deleteTestingCenterIdHandle = this.deleteTestingCenterIdHandle.bind(this);
      this.handleChange = this.handleChange.bind(this);          
  	}

  handleOnSubmit(event) {
    event.preventDefault();
    const full_name = ReactDOM.findDOMNode(this.refs.full_name).value;
    const short_name = ReactDOM.findDOMNode(this.refs.short_name).value;
    const head = ReactDOM.findDOMNode(this.refs.head).value;
    const position = ReactDOM.findDOMNode(this.refs.position).value;
    const contact = ReactDOM.findDOMNode(this.refs.contact).value;
    const adress = ReactDOM.findDOMNode(this.refs.adress).value;
    const { editSaveTestingCenterId,id } = this.props;
    var threshold = ReactDOM.findDOMNode(this.refs.threshold).value;
    if(threshold>500) threshold=500;    
    editSaveTestingCenterId({id, full_name, short_name, head, position, contact, adress, threshold});
  }

  componentDidMount() {
  	const { id , getTestingCenterId} = this.props;
     getTestingCenterId(id);
  }
  deleteTestingCenterIdHandle(){
    const {testingcenters:{testingCenterId}, deleteTestingCenterId} = this.props;
    alertify
      .okBtn("Удалить")
      .cancelBtn("Отмена")
      .confirm("Удалить тестовый центр: "+testingCenterId.full_name+"?", function (ev) {
          deleteTestingCenterId(testingCenterId._id);
      });    
  }
  handleChange(event) {
    event.preventDefault();
    if(event.target.value>500) event.target.value=500;
    else if(event.target.value<0) event.target.value=0;
    else event.target.value=Number(event.target.value);
  }    
  render() {
      const {testingcenters:{testingCenterId, editTestingCenterToggle}, editTestingCenterMode} = this.props;  
      
      return (
      <Form horizontal onSubmit={this.handleOnSubmit}>
      <h1>Тестовый центр {testingCenterId.short_name}</h1>
      <ButtonGroup>
        <Button className="btn-primary" onClick={ editTestingCenterMode }>Редактировать</Button>
        <Button className="btn-primary" onClick={ this.deleteTestingCenterIdHandle }>Удалить</Button>       
      </ButtonGroup>

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Краткое наименование центра:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="mail"
                placeholder="Enter text"
                ref="short_name"
                defaultValue={testingCenterId.short_name}
                 />
                : <h4>{testingCenterId.short_name}</h4>}
              </Col>   
          </FormGroup>

          <FormGroup controlId="formHorizontalFio">
          <Message/>
            <Col componentClass={ControlLabel} sm={2}>Полное наименование центра:</Col>
            <Col sm={3}>
              
            { editTestingCenterToggle ? 
              <textarea className="form-control" rows="3" ref="full_name" required defaultValue={testingCenterId.full_name}></textarea>
               : <h4>{testingCenterId.full_name}</h4>}
    
            </Col>
          </FormGroup>

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Руководитель центра:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="mail"
              required
                ref="head"
                defaultValue={testingCenterId.head}
                 />
                : <h4>{testingCenterId.head}</h4>}
              </Col>   
          </FormGroup>

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Должность:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="mail"
              required
                ref="position"
                defaultValue={testingCenterId.position}
                 />
                : <h4>{testingCenterId.position}</h4>}
              </Col>   
          </FormGroup>

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Адрес центра:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="mail"
              ref="adress"
                defaultValue={testingCenterId.adress}
                 />
                : <h4>{testingCenterId.adress}</h4>}
              </Col>   
          </FormGroup>          

          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Контактные данные центра:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="mail"
              ref="contact"
                defaultValue={testingCenterId.contact}
                 />
                : <h4>{testingCenterId.contact}</h4>}
              </Col>   
          </FormGroup>           
          <FormGroup controlId="formControlsEmail">
            <Col componentClass={ControlLabel} sm={2}>Порог баллов:</Col>
              <Col sm={3}>
               { editTestingCenterToggle ?
              <FormControl type="number" min="0" max="500"
              ref="threshold"
              required
                defaultValue={testingCenterId.threshold}
                onChange={this.handleChange}
                 />
                : <h4>{testingCenterId.threshold}</h4>}
              </Col>   
          </FormGroup> 

          { editTestingCenterToggle ?
          <FormGroup>
            <Col smOffset={2} sm={3}>                                 
            <Button className="btn-primary" type="submit"
            >
             Сохранить
            </Button>            
            </Col>
          </FormGroup>
          : ""}
      </Form>
      );
  }
}
TestingCenterId.propTypes = {
  userslist: PropTypes.object,
  getTestingCenterId: PropTypes.func.isRequired,
  editTestingCenterMode: PropTypes.func.isRequired,
  editSaveTestingCenterId: PropTypes.func.isRequired,
  //deleteTestingCenterIdHandle: PropTypes.func.isRequired,
  deleteTestingCenterId: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    testingcenters: state.testingcenters,
    id: ownProps.params.id
  };
}

export default connect(mapStateToProps, { getTestingCenterId, editTestingCenterMode, editSaveTestingCenterId, deleteTestingCenterId})(TestingCenterId);
