import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getUsersList, addUserMode, addUser} from '../../actions/userslist';
import { getTestingCenters } from '../../actions/testingcenters';
import UsersListTable from '../../components/Admin/UsersListTable';
import UsersListAddUser from '../../components/Admin/UsersListAddUser';
import Button from 'react-bootstrap/lib/Button';

class UsersList extends Component {
	constructor(props) {
	    super(props);
	    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  	}
  handleOnSubmit(event) {
    event.preventDefault();
    const { addUserMode }= this.props;
    addUserMode();
  }

  componentDidMount() {
  	const { getUsersList, getTestingCenters } = this.props;
     getUsersList();
     getTestingCenters();
  }
  render() {
  	const {getTestingCenters, testingcenters: { centerlist }, userslist: { userslist }, userslist: { addUserToggle }, addUserMode , addUser} = this.props;    
    if(addUserToggle){
      return (
        <div >
        <Button
          bsStyle="primary"
          onClick={ addUserMode }>
          { addUserToggle? "Список пользователей" : "Добавить пользователя" }
        </Button>
           	<UsersListAddUser 
            onEntrySave={ addUser }
            centerlist={ centerlist }
            />
        </div>
      );
    }
      return (
        <div >

          <UsersListTable
            userslist={userslist} 
            addUserMode={addUserMode}/>
        </div>
      );
  }
}
UsersList.propTypes = {
  userslist: PropTypes.object,
  getUsersList: PropTypes.func.isRequired,
  addUserMode: PropTypes.func.isRequired,
  addUser: PropTypes.func.isRequired,
  getTestingCenters: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    userslist: state.userslist,
    testingcenters: state.testingcenters
  };
}

export default connect(mapStateToProps, { addUser, addUserMode, getUsersList, getTestingCenters})(UsersList);
