import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getTestSpecies, new_id, addTestSpecies, deleteTestSpecies, editTestSpecies} from '../../actions/testspecies';
import TestSpeciesTable from '../../components/Admin/TestSpeciesTable';
import Button from 'react-bootstrap/lib/Button';
import Message from '../Message';
class TestSpecies extends Component {
	constructor(props) {
	    super(props);
	    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  	}
  handleOnSubmit(event) {
    event.preventDefault();
  }

  componentDidMount() {
    const {getTestSpecies} = this.props;
    getTestSpecies();
  }
  render() { 
  const {testspecies:{specieslist}, new_id, addTestSpecies, deleteTestSpecies, editTestSpecies} = this.props;
      return (
        <div>
          <Message/>
          <TestSpeciesTable {...this.props}/>
      </div>
      );
  }
}
TestSpecies.propTypes = {
  getTestSpecies: PropTypes.func.isRequired,
  new_id: PropTypes.func.isRequired,
  addTestSpecies: PropTypes.func.isRequired,
  deleteTestSpecies: PropTypes.func.isRequired,
  editTestSpecies: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    testspecies: state.testspecies
  };
}

export default connect(mapStateToProps, { getTestSpecies, new_id, addTestSpecies, deleteTestSpecies, editTestSpecies})(TestSpecies);
