import React, { PropTypes } from 'react';
import NavigationHead from '../containers/NavigationHead';

const App = ({children}) => {
  return (
    <div>
      <NavigationHead />
        {children}
    </div>
  );
};

App.propTypes = {
  children: PropTypes.object
};

export default App;
