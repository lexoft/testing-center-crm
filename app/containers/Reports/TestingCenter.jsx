import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getReportsTC, updatePage, updatePageSize, updateSearch, updateSelect, updateSort} from '../../actions/reports';
import TestingCenterListTable from '../../components/Reports/TestingCenterListTable';
import Button from 'react-bootstrap/lib/Button';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';


class TestingCenter extends Component {
	constructor(props) {
	    super(props);
      this.onPageChange = this.onPageChange.bind(this);
      this.onSizePerPageList = this.onSizePerPageList.bind(this);
      this.selectChange = this.selectChange.bind(this);
      this.onSortChange = this.onSortChange.bind(this);
  	}

  componentDidMount() {
  	const { getReportsTC , reports:{pageSize, page}} = this.props;
     getReportsTC(pageSize, page, "", "off");
  }
  onPageChange(page, sizePerPage) {
    const {updatePage, getReportsTC, reports:{selectCT, sort}} = this.props;
    updatePage(page);
    getReportsTC(sizePerPage, page, selectCT, sort);
  }

   onSizePerPageList(sizePerPage) {
    const {updatePageSize, updatePage, getReportsTC, reports: {page, pageSize}} = this.props;
    updatePageSize(sizePerPage);
  }
  selectChange(e){
    e.preventDefault();
    const {updatePage, getReportsTC, reports: {page, pageSize, sort}, updateSelect} = this.props;
    updateSelect(e.target.value);
    updatePage(1);
    getReportsTC(pageSize, 1, e.target.value, sort);    
  }
  onSortChange(sortName, sortOrder){
    const {updateSort, reports: {page, pageSize, selectCT}, getReportsTC} = this.props;
    getReportsTC(pageSize, 1, selectCT, sortOrder);
    updateSort(sortOrder);
    if (sortOrder === 'asc') {      
      return 1;
    } else {
      return 0;
    }
  }
  render() {
  	const {reports: { list, totalSize, page, pageSize },user,  user:{ profile }} = this.props;    
      return (  
        <div>      
          <TestingCenterListTable
            list={list}
            testing_center={profile.testing_center}
            onPageChange={ this.onPageChange } 
            totalSize={totalSize}
            page={page}
            pageSize={pageSize}
            onSizePerPageList={ this.onSizePerPageList }
            selectChange={this.selectChange}
            onSortChange={this.onSortChange}
            user={user}
            />
        </div>
      );
  }
}
TestingCenter.propTypes = {
  getReportsTC: PropTypes.func.isRequired,
  updatePage: PropTypes.func.isRequired,
  updatePageSize: PropTypes.func.isRequired,
  updateSelect: PropTypes.func.isRequired,
  updateSort: PropTypes.func.isRequired,
};


function mapStateToProps(state) {
  return {
    reports: state.reports,
    user: state.user,
  };
}

export default connect(mapStateToProps, { getReportsTC, updatePage, updatePageSize, updateSelect, updateSort})(TestingCenter);
