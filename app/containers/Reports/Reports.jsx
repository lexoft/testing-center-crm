import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { getReports, updatePage, updatePageSize, updateSearch, updateSelect, updateSort} from '../../actions/reports';
import {Col, Button, ControlLabel, FormControl} from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import { defaultRanges, Calendar, DateRange } from 'react-date-range';
import '../../css/reports.css';
import moment from 'moment';
import Select from 'react-select';
import '../../css/react-select.css';

class Section extends Component {
  render() {
    return (
      <div >
        <div >
          <div className='Demo-inputs'>
            { this.props.children[0] }
          </div>
        </div>
        <div>
          { this.props.children[1] }
        </div>
      </div>
    )
  }
}

class Reports extends Component {
	constructor(props) {
	  super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.updateTestingCenterValue = this.updateTestingCenterValue.bind(this);
    this.updateTypeValue = this.updateTypeValue.bind(this);

    this.state = {
      'rangePicker' : {},
      'linked' : {},
      'datePicker' : null,
      'firstDayOfWeek' : null,
      'predefined' : {},
      'testing_center': 'all',
      'type':'certificates' 
    }
  }
handleOnSubmit(event) {
    event.preventDefault();
    //const {}=this.state;
    const testing_center= this.state.testing_center;
    const type= this.state.type;
    window.open("/reports/"+type+"/"+testing_center+"/"+this.state.linked['startDate']+"/"+this.state.linked['endDate']);
  }
  handleChange(which, payload) {
    this.setState({
      [which] : payload
    });
  }
  updateTestingCenterValue (newValue) {
      this.setState({
        testing_center: newValue
      });
  }
  updateTypeValue (newValue) {
      this.setState({
        type: newValue
      });
  }
  render() {
    moment.updateLocale('ru', {
    months : [
        "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
        "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    ],
    weekdaysMin : ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
    weekdays : {
        standalone: 'Воскресенье_Понедельник_Вторник_Среда_Четверг_Пятница_Суббота'.split('_'),
        format: 'Воскресенье_Понедельник_Вторник_Среду_Четверг_Пятницу_Субботу'.split('_'),
        isFormat: /\[ ?[Вв] ?(?:прошлую|следующую|эту)? ?\] ?dddd/
      }  
    });
  	const {user,  user:{ profile }} = this.props; 
    const { rangePicker, linked, datePicker, firstDayOfWeek, predefined} = this.state;
    const format = 'DD.MM.YYYY';
    var opt=[{'id':'all', 'name':'Все'}];
    opt=opt.concat(user['profile']['testing_center']);    
      return (
      <div>
      <Col md={12} className="reports-header">
      <h1>Генерация отчетов</h1>
      <hr/>
      </Col>
       <Section>
       <Col md={3}>
       <form onSubmit={this.handleOnSubmit}>
            <ControlLabel>Тип отчета:</ControlLabel>
                  <Select 
                    options={[
                      {'value':'certificates', 'label':'Реестр выданных сертификатов'},
                      {'value':'testing_center', 'label':'Аналитика по центрам тестирования'},
                      {'value':'summary_table', 'label':'Сводная таблица'},
                      ]} 
                    value={this.state.type}
                    onChange={this.updateTypeValue}
                    placeholder=''
                    simpleValue  searchable={false} clearable={false}
                  />                              
            <ControlLabel>Диапозон даты:</ControlLabel>
                <FormControl type="text"
                readOnly
                value={ linked['startDate'] && linked['startDate'].format(format).toString() }                
                   /> 
                <FormControl type="text"
                readOnly
                value={ linked['endDate'] && linked['endDate'].format(format).toString() }
                   />
              <ControlLabel>Центр тестирования:</ControlLabel>
                  <Select 
                    options={opt} 
                    labelKey="name" valueKey="id"
                    value={this.state.testing_center}
                    onChange={this.updateTestingCenterValue}
                    placeholder=''
                    simpleValue  searchable={false} clearable={false}
                  />  
              <Button style={{'width':'100%'}}  className="btn-primary reports-btn" type="submit">Сгенерировать</Button>            
        </form>
        </Col>
        <Col md={9}>
          <DateRange
          shownDate={moment()}
            startDate={ now => {
              return(now);
            }}
            endDate={ now => {
              return(now);
            }}
            firstDayOfWeek={ 1 }
            linkedCalendars={ false }
            onInit={ this.handleChange.bind(this, 'linked') }
            onChange={ this.handleChange.bind(this, 'linked') }
          />
        </Col>
        </Section>          

        </div>
      );
  }
}
Reports.propTypes = {
  getReports: PropTypes.func.isRequired,
};


function mapStateToProps(state) {
  return {
    reports: state.reports,
    user: state.user,
  };
}

export default connect(mapStateToProps, { getReports})(Reports);
