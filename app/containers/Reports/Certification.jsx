import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getReports, updatePage, updatePageSize, updateSearch, updateSelect, updateSort} from '../../actions/reports';
import CertificationListTable from '../../components/Reports/CertificationListTable';
import Button from 'react-bootstrap/lib/Button';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';


class Certification extends Component {
	constructor(props) {
	    super(props);
      this.onPageChange = this.onPageChange.bind(this);
      this.onSizePerPageList = this.onSizePerPageList.bind(this);
      this.selectChange = this.selectChange.bind(this);
      this.onSortChange = this.onSortChange.bind(this);
  	}

  componentDidMount() {
  	const { getReports , reports:{pageSize, page}} = this.props;
     getReports(pageSize, page, "", "off");
  }
  onPageChange(page, sizePerPage) {
    const {updatePage, getReports, reports:{selectCT, sort}} = this.props;
    updatePage(page);
    getReports(sizePerPage, page, selectCT, sort);
  }

   onSizePerPageList(sizePerPage) {
    const {updatePageSize, updatePage, getReports, reports: {page, pageSize}} = this.props;
    updatePageSize(sizePerPage);
  }
  selectChange(e){
    e.preventDefault();
    const {updatePage, getReports, reports: {page, pageSize, sort}, updateSelect} = this.props;
    updateSelect(e.target.value);
    updatePage(1);
    getReports(pageSize, 1, e.target.value, sort);    
  }
  onSortChange(sortName, sortOrder){
    const {updateSort, reports: {page, pageSize, selectCT}, getReports} = this.props;
    getReports(pageSize, 1, selectCT, sortOrder);
    updateSort(sortOrder);
    if (sortOrder === 'asc') {      
      return 1;
    } else {
      return 0;
    }
  }
  render() {
  	const {reports: { list, totalSize, page, pageSize },user,  user:{ profile }} = this.props;    
      return (  
        <div>      
          <CertificationListTable
            list={list}
            testing_center={profile.testing_center}
            onPageChange={ this.onPageChange } 
            totalSize={totalSize}
            page={page}
            pageSize={pageSize}
            onSizePerPageList={ this.onSizePerPageList }
            selectChange={this.selectChange}
            onSortChange={this.onSortChange}
            user={user}
            />
        </div>
      );
  }
}
Certification.propTypes = {
  getReports: PropTypes.func.isRequired,
  updatePage: PropTypes.func.isRequired,
  updatePageSize: PropTypes.func.isRequired,
  updateSelect: PropTypes.func.isRequired,
  updateSort: PropTypes.func.isRequired,
};


function mapStateToProps(state) {
  return {
    reports: state.reports,
    user: state.user,
  };
}

export default connect(mapStateToProps, { getReports, updatePage, updatePageSize, updateSelect, updateSort})(Certification);
