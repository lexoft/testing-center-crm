import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getReports, saveReports, printReports} from '../../actions/reports';
import {Col, Button, ControlLabel} from 'react-bootstrap';
import CertificationListTable from '../../components/Reports/CertificationListTable';
import TestingCenterListTable from '../../components/Reports/TestingCenterListTable';
import SummaryListTable from '../../components/Reports/SummaryListTable';
import moment from 'moment';

class ReportsGenerate extends Component {
	constructor(props) {
	    super(props);
  }
  componentDidMount() {
    document.getElementsByTagName("nav")[0].classList.add('hidden');
    const { getReports, testing_center, type, startDate, endDate} = this.props;
    getReports(type, testing_center, startDate, endDate);
  }
  selectType(){
    const {reports: { list}, testing_center, startDate, endDate, type, user} = this.props;
    if(type=="certificates") return(<CertificationListTable list={list} startDate={startDate} endDate={endDate}></CertificationListTable>);
    if(type=="testing_center") return(<TestingCenterListTable list={list} startDate={startDate} endDate={endDate} testing_centers={user.profile.testing_center}></TestingCenterListTable>);
    if(type=="summary_table") return(<SummaryListTable list={list} startDate={startDate} endDate={endDate} testing_centers={user.profile.testing_center}></SummaryListTable>);

  }
  dateFormatter(data) {
    var date = new Date(data);
    var day=date.getDate();
    var month=date.getMonth()+1;
    var year=date.getFullYear();
    if(day<10) day='0'+day;
    if(month<10) month='0'+month;
    return (day+'.'+month+'.'+year);
  } 
  render() {
    const {printReports, saveReports, testing_center, startDate, endDate, type}=this.props;
      return (
      <div>
      <div className="reports-body">
        <Col md={12} className="reports-result">
        {this.selectType()}
        </Col>
      </div>
        <div className="navbar-fixed-bottom row-fluid repors-fix-footer">
                  <div className="container" style={{marginLeft:0}}>
                            <Button style={{margin:"10px"}} className="btn-success" type="submit" onClick={()=>saveReports(type, testing_center, this.dateFormatter(new Date(+startDate)), this.dateFormatter(new Date(+endDate)),document.getElementsByClassName("reports-body")[0])}>Сохранить</Button>
                            <Button style={{margin:"10px"}} className="btn-primary" type="submit" onClick={printReports}>Распечатать</Button>
                  </div>
        </div>
      </div>      
      );
  }
}
ReportsGenerate.propTypes = {
  getReports: PropTypes.func.isRequired,
  saveReports: PropTypes.func.isRequired,
  printReports: PropTypes.func.isRequired,
};


function mapStateToProps(state, ownProps) {
  return {
    reports: state.reports,
    user: state.user,
    testing_center: ownProps.params.testing_center,
    startDate: ownProps.params.startDate,
    endDate: ownProps.params.endDate,
    type: ownProps.params.type
  };
}

export default connect(mapStateToProps, { getReports, saveReports, printReports})(ReportsGenerate);
