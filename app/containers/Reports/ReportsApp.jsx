import React, { PropTypes } from 'react';
import Message from '../../containers/Message';
import Col from 'react-bootstrap/lib/Col'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import NavigationMenu from './Navigation'

const ReportsApp = ({children}) => {
  return (
    <Row className="show-grid">
	        {children}
    </Row>
  );
};

ReportsApp.propTypes = {
  children: PropTypes.object
};

export default ReportsApp;
