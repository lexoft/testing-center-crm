import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Message from './Message';
import { manualLogin, password_reset} from '../actions/users';
import {FormGroup, FormControl, Button, Col, Checkbox, Form, ControlLabel} from 'react-bootstrap';
import '../css/Login.css';
class InquiryPassword extends Component {

  constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleOnSubmit(event) {
    event.preventDefault();
    const {password_reset} = this.props;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    password_reset({ email });
  }


  render() {
    
    const {message} = this.props.user;
    return (
<div className="vertical-center">
  <div className="form_login">      
          <Form horizontal onSubmit={this.handleOnSubmit}>
            <h2>Восстановление пароля:</h2>
            <FormGroup controlId="formHorizontalEmail">
                <FormControl type="email" placeholder="Email" ref="email"/>
            </FormGroup>
            <FormGroup>
                <Button type="submit" className="btn-primary btn-block">
                  Отправить
                </Button>
              <Col className="loginReset" smOffset={1} sm={9}>
                          <Link to="/login">Войти в систему</Link> {message}
              </Col>              
            </FormGroup>
          </Form>
  </div>
        <div  className="form_login">
        <Message/>
        </div>  
</div>       
    );
  }
}

InquiryPassword.propTypes = {
  password_reset: PropTypes.func.isRequired,
};


function mapStateToProps({user}) {
  return {
    user
  };
}


export default connect(mapStateToProps, {password_reset })(InquiryPassword);

