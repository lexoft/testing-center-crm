import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import { dismissMessage, alertShow } from '../actions/messages';
import {Alert} from 'react-bootstrap'


const Message = ({message, type, print, dismissMessage, alertShow}) => (
	<div>
		{ message.length>0 ?
			 (type=="SUCCESS" ?
			 	  (print=="message" ? 
			 	  		<Alert bsStyle="success">
					        <h4 onClick={dismissMessage}>{ message }</h4>
					    </Alert> : alertShow(message, type) )

			 	    :
			 	    (print=="message" ? 
			 	            <Alert bsStyle="danger">
					          <h4 onClick={dismissMessage}>{ message }</h4>
					        </Alert> : alertShow(message, type))
			 )
		: ""}
	  </div>

);

Message.propTypes = {
  message: PropTypes.string,
  type: PropTypes.string,
  print: PropTypes.string,
  dismissMessage: PropTypes.func.isRequired,
  alertShow: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
	
  return {...state.message};
}

export default connect(mapStateToProps, { dismissMessage, alertShow })(Message);
