import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { manualLogin, password_reset} from '../actions/users';
import Message from './Message';
import {FormGroup, FormControl, Button, Col, Checkbox, Form, ControlLabel} from 'react-bootstrap';
import '../css/Login.css';
class Login extends Component {

  constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleOnSubmit(event) {
    event.preventDefault();
    
    const { manualLogin, user: { isLogin } } = this.props;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    const password = ReactDOM.findDOMNode(this.refs.password).value;
    manualLogin({ email, password });
  }


  render() {
    
    const { isWaiting, message, isLogin, } = this.props.user;
    return (
<div className="vertical-center">
  <div className="form_login">
        <Col md={12}>
          <Form horizontal onSubmit={this.handleOnSubmit}>
            <h2>Вход в систему</h2>
            <FormGroup controlId="formHorizontalEmail">
                <FormControl type="email" placeholder="Email" ref="email"/>
            </FormGroup>
            <FormGroup controlId="formHorizontalPassword">
                <FormControl type="password" placeholder="Пароль" ref="password"/>
            </FormGroup>
            <FormGroup>
                <Button type="submit" className="btn-primary btn-block">
                  Войти
                </Button>
              <Col className="loginReset" sm={12}>
                          <Link to="/password_reset">Забыли пароль</Link> 
              </Col>              
            </FormGroup>
          </Form>
        </Col>
  </div>
        <div  className="form_login">
        <Message/>
        </div>  
</div>          
    );
  }
}

Login.propTypes = {
  user: PropTypes.object,
  manualLogin: PropTypes.func.isRequired,
  password_reset: PropTypes.func.isRequired
};


function mapStateToProps({user}) {
  return {
    user
  };
}

export default connect(mapStateToProps, { manualLogin })(Login);

