import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import Message from './Message';
import { connect } from 'react-redux';
import { manualLogin, password_reset, toggleLoginMode, identificationResetToken, new_password } from '../actions/users';
import {FormGroup, Alert, FormControl, Button, Col, Checkbox, Form, ControlLabel} from 'react-bootstrap';
class PasswordReset extends Component {
  constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleOnSubmit(event) {
    event.preventDefault();
    const {new_password, token } = this.props;
    const password = ReactDOM.findDOMNode(this.refs.password).value;
    new_password({ token, password });
  }

  componentDidMount(){
    const { identificationResetToken } = this.props;
    const { token } = this.props;
    identificationResetToken({token});
  }

  render() {
    
    const { isWaiting, isLogin, identification_resettoken } = this.props.user;
    const {toggleLoginModem, token } = this.props;
    if(identification_resettoken){
      return (
<div className="vertical-center">
  <div className="form_login">
        <Form horizontal onSubmit={this.handleOnSubmit}>
        <h2>Восстановление пароля</h2>
     <FormGroup controlId="formHorizontalPassword">
              <FormControl type="password" placeholder="Новый пароль" ref="password" required/>
          </FormGroup>
          <FormGroup>
              <Button type="submit" className="btn-primary btn-block">
                Отправить
              </Button>
          </FormGroup>
        </Form>
  </div> 
</div>          
          ); 
    } else {
        return(<Alert bsStyle="danger"><h4>Неверный секретный ключ. Для повторного отправления секретного ключа, перейдите на страницу <Link to="password_reset">восстановления пароля</Link></h4></Alert>);
    }

  }
}

PasswordReset.propTypes = {
  user: PropTypes.object,
  manualLogin: PropTypes.func.isRequired,
  password_reset: PropTypes.func.isRequired,
  toggleLoginMode: PropTypes.func.isRequired,
  identificationResetToken: PropTypes.func.isRequired,
  new_password: PropTypes.func.isRequired,
};


function mapStateToProps({user}, ownProps) {
  return {
    user,
    token: ownProps.params.token
  };
}


export default connect(mapStateToProps, { new_password, manualLogin, password_reset, toggleLoginMode, identificationResetToken })(PasswordReset);

