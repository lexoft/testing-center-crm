/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';
import { push } from 'react-router-redux';
polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeTestingCentersRequest(method, data, api = '/api/testing_centers') {
  return request[method](api, data);
}

function addTestingCenterSuccess(){
  return{
    type: types.ADD_TESTINGCENTERS_SUCCESS
  };
}

function addTestingCenterError(message){
  return{
    type: types.ADD_TESTINGCENTERS_FAILURE,
    message
  };
}

function beginLoad(){
	return { type: types.GET_TESTINGCENTERS}
}

function testingCentersSuccess(json){
	return{
		type: types.GET_TESTINGCENTERS_SUCCESS,
		data: json
	};
}

function testingCentersError(message){
	return{
		type: types.GET_TESTINGCENTERS_FAILURE,
		message
	};
}
function beginGetTestingCenterId(){
  return{
    type: types.GET_TESTINGCENTERID
  };
}
function getTestingCenterIdSuccess(data){
  return{
    type: types.GET_TESTINGCENTERID_SUCCESS,
    data
  };
}
function getTestingCenterIdError(message){
  return{
    type: types.GET_TESTINGCENTERID_FAILURE,
    message
  };
}
function editTestingCenterIdSuccess(message){
  return{
    type: types.EDIT_TESTINGCENTERID_SUCCESS,
    message: message
  };
}
function editTestingCenterIdError(message){
  return{
    type: types.EDIT_TESTINGCENTERID_FAILURE,
    message: message
  };
}
function deleteTestingCenterIdSuccess(message){
  return{
    type: types.DELETE_TESTINGCENTERID_SUCCESS,
    message
  };
}
function deleteTestingCenterIdError(message){
  return{
    type: types.DELETE_TESTINGCENTERID_FAILURE,
    message
  };
}
export function editTestingCenterMode() {
  return { type: types.EDIT_TESTINGCENTERID };
}
export function addTestingCenterMode() {
  return { type: types.ADD_TESTINGCENTERS };
}
export function deleteTestingCenterId(data) {
  return dispatch => {
    return makeTestingCentersRequest('delete', {}, '/api/testing_center/'+data)
      .then(response => {
        if (response.status === 200) {
          dispatch(deleteTestingCenterIdSuccess(response.data.message));  
          dispatch(push("/admin/testing_centers"));
        } else {
          dispatch(deleteTestingCenterIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {        
        alertify.alert(err.response.data.message);
        //dispatch(deleteTestingCenterIdError(getMessage(err)));
      });
  };
}

export function editSaveTestingCenterId(data) {
  return dispatch => {
    return makeTestingCentersRequest('put', data, '/api/testing_center')
      .then(response => {
        if (response.status === 200) {
          dispatch(editTestingCenterIdSuccess(response.data.message));  
          dispatch(getTestingCenterId(data.id));
        } else {
          dispatch(editTestingCenterIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(editTestingCenterIdError(getMessage(err)));
      });
  };
}
export function getTestingCenterId(data) {
  return dispatch => {
    dispatch(beginGetTestingCenterId());
    return makeTestingCentersRequest('get', {}, '/api/testing_center/'+data)
      .then(response => {
        if (response.status === 200) {
          dispatch(getTestingCenterIdSuccess(response.data));                
        } else {
          dispatch(getTestingCenterIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {        
        dispatch(getTestingCenterIdError(getMessage(err)));
        dispatch(push("/admin/testing_centers"));
      });
  };
}


export function addTestingCenter(data) {
  return dispatch => {
    return makeTestingCentersRequest('post', data, '/api/testing_center')
      .then(response => {
        if (response.status === 200) {
          dispatch(addTestingCenterSuccess());  
          dispatch(getTestingCenters());
        } else {
          dispatch(addTestingCenterError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(addTestingCenterError('Oops! Something went wrong!'));
      });
  };
}

export function getTestingCenters(data) {
  return dispatch => {
    dispatch(beginLoad());
    return makeTestingCentersRequest('get', '/api/testing_centers')
      .then(response => {
        if (response.status === 200) {
          dispatch(testingCentersSuccess(response.data));
        } else {
          dispatch(testingCentersError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(testingCentersError('Oops! Something went wrong!'));
      });
  };
}



