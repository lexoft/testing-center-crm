/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';
import { push } from 'react-router-redux';

polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeUsersListRequest(method, data, api = '/api/userslist') {
  return request[method](api, data);
}

function addUserSuccess(message){
  return{
    type: types.ADD_USERSLIST_SUCCESS,
    message
  };
}

function addUserError(message){
  return{
    type: types.ADD_USERSLIST_FAILURE,
    message
  };
}

function beginLoad(){
	return { type: types.GET_USERSLIST}
}

function usersListSuccess(json){
	return{
		type: types.GET_USERSLIST_SUCCESS,
		data: json
	};
}

function usersListError(){
	return{
		type: types.GET_USERSLIST_FAILURE
	};
}
function beginGetUserId(){
  return{
    type: types.GET_USERID
  };
}
function getUserIdSuccess(data){
  return{
    type: types.GET_USERID_SUCCESS,
    data
  };
}
function getUserIdError(message){
  return{
    type: types.GET_USERID_FAILURE,
    message
  };
}
function editUserIdSuccess(message){
  return{
    type: types.EDIT_USERID_SUCCESS,
    message: message
  };
}
function editUserIdError(message){
  return{
    type: types.EDIT_USERID_FAILURE,
    message: message
  };
}
function deleteUserIdSuccess(message){
  return{
    type: types.DELETE_USERID_SUCCESS,
    message
  };
}
function deleteUserIdError(message){
  return{
    type: types.DELETE_USERID_FAILURE,
    message
  };
}
function blockUserIdSuccess(message){
  return{
    type: types.BLOCK_USERID_SUCCESS,
    message
  };
}
function blockUserIdError(message){
  return{
    type: types.BLOCK_USERID_FAILURE,
    message
  };
}
export function editUserMode() {
  return { type: types.EDIT_USERID };
}
export function addUserMode() {
  return { type: types.ADD_USERSLIST };
}

export function blockUserId(data) {
  return dispatch => {
    return makeUsersListRequest('post', data, '/api/blockuserid')
      .then(response => {
        if (response.status === 200) {
          dispatch(blockUserIdSuccess(response.data.message));
          if(data.block.length==1){
            dispatch(getUserId(data.block[0]));
          } else {
            dispatch(getUsersList());
          }
        } else {
          dispatch(blockUserIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(blockUserIdError(getMessage(err)));
      });
  };
}

export function deleteUserId(data) {
  return dispatch => {
    return makeUsersListRequest('delete', {}, '/api/user/'+data.id)
      .then(response => {
        if (response.status === 200) {
          dispatch(deleteUserIdSuccess(response.data.message));  
          dispatch(push("/admin/users"));
        } else {
          dispatch(deleteUserIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(deleteUserIdError(getMessage(err)));
      });
  };
}

export function editSaveUserId(data) {
  return dispatch => {
    return makeUsersListRequest('put', data, '/api/user')
      .then(response => {
        if (response.status === 200) {
          dispatch(editUserIdSuccess(response.data.message));  
          dispatch(getUserId(data.id));
        } else {
          dispatch(editUserIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(editUserIdError(getMessage(err)));
      });
  };
}

export function addUser(data) {
  return dispatch => {
    return makeUsersListRequest('post', data, '/api/user')
      .then(response => {
        if (response.status === 200) {
          dispatch(addUserSuccess(response.data.message));  
          dispatch(getUsersList());
        } else {
          dispatch(addUserError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(addUserError(getMessage(err)));
      });
  };
}

export function getUsersList() {
  return dispatch => {
    dispatch(beginLoad());
    return makeUsersListRequest('get', '/api/userslist')
      .then(response => {
        if (response.status === 200) {
          dispatch(usersListSuccess(response.data));                
        } else {
          dispatch(usersListError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(usersListError('Oops! Something went wrong!'));
      });
  };
}


export function getUserId(data) {
  return dispatch => {
    return makeUsersListRequest('get', {}, '/api/user/'+data)
      .then(response => {
        if (response.status === 200) {
          dispatch(getUserIdSuccess(response.data));                
        } else {
          dispatch(getUserIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {        
        dispatch(getUserIdError(getMessage(err)));
        dispatch(push("/admin/users"));
      });
  };
}


