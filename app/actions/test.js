/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';
import { push } from 'react-router-redux';
import {saveAs} from 'file-saver';
import fetch from 'isomorphic-fetch';
polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeTestRequest(method, data, api = '/api/userslist', config) {
  return request[method](api, data, config);
}

function addDataSuccess(message){
  return{
    type: types.ADD_TEST_DATA_SUCCESS,
    message
  };
}

function addDataError(message){
  return{
    type: types.ADD_TEST_DATA_FAILURE,
    message
  };
}

function beginGetTestById(){
  return{
    type: types.GET_TEST_BY_ID,
  };
}

function getTestByIdSuccess(data){
  return{
    type: types.GET_TEST_BY_ID_SUCCESS,
    data
  };
}

function getTestByIdError(message){
  return{
    type: types.GET_TEST_BY_ID_ERROR,
    message
  };
}

export function addTestScansSuccess(message){
  return{
    type: types.ADD_TEST_SCANS_SUCCESS,
    message
  };
}
export function addTestScansError(message){
  return{
    type: types.ADD_TEST_SCANS_ERROR,
    message
  };
}

function deleteTestScansSucces(message){
  return{
    type: types.DELETE_TEST_SCANS_SUCCESS,
    message
  };
}

function deleteTestScansError(message){
  return{
    type: types.DELETE_TEST_SCANS_ERROR,
    message
  };
}

function updateCertificationSuccess(message) {
  return{
    type: types.UPDATE_TEST_CERTIFICATION_SUCCESS,
    message
  };
}

function updateCertificationError(message){
  return{
    type: types.UPDATE_TEST_CERTIFICATION_ERROR,
    message
  };
}


function beginLoadTestList(){
  return { type: types.GET_TEST_LIST}
}

function getTestListSuccess(json){
  return{
    type: types.GET_TEST_LIST_SUCCESS,
    data: json
  };
}

function getTestListError(message){
  return{
    type: types.GET_TEST_LIST_FAILURE,
    message
  };
}
export function updatePage(data){
  return{
    type: types.UPDATE_TEST_LIST_PAGINATION,
    data
  };
}

export function updatePageSize(data){
  return{
    type: types.UPDATE_TEST_LIST_PAGE_SIZE,
    data
  };
}

export function updateSearch(data){
  return{
    type: types.UPDATE_TEST_LIST_SEARCH,
    data
  };
}

export function updateSelect(data){
  return{
    type: types.UPDATE_TEST_LIST_SELECT,
    data
  };
}

export function updateDays(data){
  return{
    type: types.UPDATE_TEST_LIST_DAYS,
    data
  };
}

export function updateSort(data){
  return{
    type: types.UPDATE_TEST_LIST_SORT,
    data
  };
}

function updateTotalSize(data){
  return{
    type: types.UPDATE_TEST_LIST_TOTAL_SIZE,
    data
  };
}

function newNumber(data){
  return{
    type: types.ADD_TEST_NEW_NUMBER,
    data
  };
}
function deleteTestIdSuccess(message){
  return{
    type: types.DELETE_TEST_BY_ID_SUCCESS,
    message
  };
}

function deleteTestIdError(message){
  return{
    type: types.DELETE_TEST_BY_ID_ERROR,
    message
  };
}

function beginGeneratePdf(message){
  return{
    type: types.GENERATE_PDF,
    message
  };
}

function generatePdfSuccess(message){
  return{
    type: types.GENERATE_PDF_SUCCESS,
    message
  };
}

function generatePdfError(message){
  return{
    type: types.GENERATE_PDF_ERROR,
    message
  };
}
export function getNewNumber(data){
  return dispatch => {
    return makeTestRequest('post', data, '/api/test_new_number')
      .then(response => {
        if (response.status === 200) {
          dispatch(newNumber(response.data.message));
        } else {
          //dispatch(updateCertificationError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        //dispatch(updateCertificationError(getMessage(err)));
      });
  };
}

export function getTestList(pageSize, page, search, selectCT, days, sort) {
  return dispatch => {
    dispatch(beginLoadTestList());
    var data={
      'pageSize': pageSize,
      'page': page,
      'search': search,
      'selectCT': selectCT,
      'days': days,
      'sort':sort || ''
    }
    return makeTestRequest('post', data, '/api/testlist')
      .then(response => {
        if (response.status === 200) {
          dispatch(getTestListSuccess(response.data));
          dispatch(updateTotalSize(response.data.totalSize));
        } else {
          dispatch(getTestListError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(getTestListError('Oops! Something went wrong!'));
      });
  };
}
export function generatePDF(data, number) {
  return dispatch => {
dispatch(beginGeneratePdf());
var config={
  responseType: 'arraybuffer',
}
    return makeTestRequest('post', {'id':data}, '/api/generatepdf', config)
      .then(response => {
        if (response.status === 200) {       
          var blob = new Blob([response.data], {type: "application/pdf;charset=utf-8"});
          saveAs(blob, number+".pdf");
          dispatch(generatePdfSuccess(response));

        } else {
          dispatch(getGeneratePdf('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(generatePdfError('Oops! Something went wrong!'));
      });
  };
}

export function deleteTestId(data) {
  return dispatch => {
    return makeTestRequest('delete', {}, '/api/tested/'+data._id)
      .then(response => {
        if (response.status === 200) {
          dispatch(deleteTestIdSuccess(response.data.message));
          dispatch(push("/tested"));
        } else {
          dispatch(deleteTestIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        //console.log(err);
        dispatch(deleteTestIdError(getMessage(err)));
      });
  };
}


export function updateCertification(data){
  return dispatch => {
    return makeTestRequest('post', data, '/api/certification_update')
      .then(response => {
        if (response.status === 200) {
          dispatch(updateCertificationSuccess(response.data.message));
          dispatch(getTestById({'id': data.id}, false));
        } else {
          dispatch(updateCertificationError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(updateCertificationError(getMessage(err)));
      });
  };
}

export function deleteTestScans(data){
  return dispatch => {
    return makeTestRequest('post', data, '/api/delete_test_scans')
      .then(response => {
        if (response.status === 200) {          
          dispatch(deleteTestScansSucces(response.data.message));
          dispatch(getTestById({'id':data._id})); 
         // dispatch(push("/tested/test/"+response.data.id));          
        } else {
          dispatch(deleteTestScansError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(deleteTestScansError(getMessage(err)));
      });
  };
}


export function getTestById(data, load=true){
  return dispatch => {
    if(load){
      dispatch(beginGetTestById());
    }
    return makeTestRequest('post', data, '/api/get_test_by_id')
      .then(response => {
        if (response.status === 200) {
          
          dispatch(getTestByIdSuccess(response.data));

        } else {
          dispatch(getTestByIdError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(push("/tested"));
        dispatch(getTestByIdError(getMessage(err)));
      });
  };
}

export function uploadFile(id, file){
  return dispatch => {
        let formData = new FormData();
        formData.append("id", id);
        formData.append("file", file);
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        const url = '/api/uploadfile';
        request.post(url, formData, config)
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });   
  }
}
export function addData(data, step=true){
  return dispatch => {
    return makeTestRequest('post', data, '/api/test_add')
      .then(response => {
        if (response.status === 200) {
          dispatch(addDataSuccess(response.data.message));
          if(data._id=="new"){ 
            dispatch(getTestById({'id':response.data.id})); 
            dispatch(push("/tested/test/"+response.data.id));
          if(step){
                dispatch(NavStep(2));
              }
          } else {
            getTestById({'id': data._id}, false);
          }
        } else {
          dispatch(addDataError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(addDataError(getMessage(err)));
      });
  };
}

export function NavStep(data){
    return{
      type: types.TEST_CHANGENAV,
      data
    };
}