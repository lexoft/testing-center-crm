/* eslint consistent-return: 0, no-else-return: 0*/
import * as types from '../types';

export function dismissMessage() {
  return { type: types.DISMISS_MESSAGE };
}

export function alertShow(message, type){
	return dispatch => {
		if(type=="SUCCESS")
			alertify.success(message);
		else alertify.error(message);
		dispatch(dismissMessage());
	};
}
