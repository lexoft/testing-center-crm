/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';
import { push } from 'react-router-redux';

polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeTestSpeciesRequest(method, data, api = '/api/species') {
  return request[method](api, data);
}

function addTestSpeciesSuccess(message){
  return{
    type: types.ADD_TEST_SPECIES_SUCCESS,
    message
  };
}

function addTestSpeciesError(message){
  return{
    type: types.ADD_TEST_SPECIES_ERROR,
    message
  };
}

function beginLoad(){
	return { type: types.GET_TEST_SPECIES}
}

function testSpeciesSuccess(json){
	return{
		type: types.GET_TEST_SPECIES_SUCCESS,
		data: json
	};
}

function testSpeciesError(){
	return{
		type: types.GET_TEST_SPECIES_ERROR
	};
}

function editTestSpeciesSuccess(message){
  return{
    type: types.EDIT_TEST_SPECIES_SUCCESS,
    message: message
  };
}
function editTestSpeciesError(message){
  return{
    type: types.EDIT_TEST_SPECIES_ERROR,
    message: message
  };
}
function deleteTestSpeciesSuccess(message){
  return{
    type: types.DELETE_TEST_SPECIES_SUCCESS,
    message
  };
}
function deleteTestSpeciesError(message){
  return{
    type: types.DELETE_TEST_SPECIES_ERROR,
    message
  };
}

export function getTestSpecies() {
  return dispatch => {
    dispatch(beginLoad());
    return makeTestSpeciesRequest('get')
      .then(response => {
        if (response.status === 200) {
          dispatch(testSpeciesSuccess(response.data));                
        } else {
          dispatch(testSpeciesError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(testSpeciesError('Oops! Something went wrong!'));
      });
  };
}

export function new_id() {
  return dispatch => {
    return makeTestSpeciesRequest('get', {}, "/api/new_id_species")
      .then(response => {
        if (response.status === 200) {
          return(response.data.message);               
        } else {
          dispatch(testSpeciesError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(testSpeciesError('Oops! Something went wrong!'));
      });
  };
}

export function addTestSpecies(data) {
  return dispatch => {
    return makeTestSpeciesRequest('post', data, '/api/species')
      .then(response => {
        if (response.status === 200) {
          dispatch(addTestSpeciesSuccess(response.data.message));  
          dispatch(getTestSpecies());
        } else {
          dispatch(addTestSpeciesError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(addTestSpeciesError(getMessage(err)));
      });
  };
}

export function deleteTestSpecies(data) {
  return dispatch => {
    return makeTestSpeciesRequest('post', data, '/api/species_del')
      .then(response => {
        if (response.status === 200) {
          dispatch(deleteTestSpeciesSuccess(response.data.message));  
        } else {
          dispatch(deleteTestSpeciesError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(deleteTestSpeciesError(getMessage(err)));
      });
  };
}

export function editTestSpecies(data) {
  return dispatch => {
    return makeTestSpeciesRequest('put', data, '/api/species')
      .then(response => {
        if (response.status === 200) {
          dispatch(editTestSpeciesSuccess(response.data.message));  
        } else {
          dispatch(editTestSpeciesError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(editTestSpeciesError(getMessage(err)));
      });
  };
}


