import { polyfill } from 'es6-promise';
import request from 'axios';
import { push } from 'react-router-redux';

import * as types from '../types';

polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeUserRequest(method, data, api = '/login') {
  return request[method](api, data);
}



export function beginLogin() {
  return { type: types.MANUAL_LOGIN_USER };
}

export function loginSuccess(json) {
  return {
    type: types.LOGIN_SUCCESS_USER,
    message: json.message,
    profile: json.profile
  };
}

export function loginError(message) {
  return {
    type: types.LOGIN_ERROR_USER,
    message
  };
}


export function passwordResetError(message) {
  return {
    type: types.PASSWORD_RESET_ERROR_USER,
    message
  };
}

export function beginPasswordReset() {
  return { type: types.PASSWORD_RESET_USER };
}

export function passwordResetSuccess(message) {
  return {
    type: types.PASSWORD_RESET_SUCCESS_USER,
    message
  };
}


export function beginLogout() {
  return { type: types.LOGOUT_USER};
}

export function logoutSuccess() {
  return { type: types.LOGOUT_SUCCESS_USER };
}

export function logoutError() {
  return { type: types.LOGOUT_ERROR_USER };
}

export function toggleLoginMode() {
  return { type: types.TOGGLE_LOGIN_MODE };
}

export function beginIdenResetToken() {
  return { type: types.IDENTIFICATION_RESET_TOKEN_USER };
}
export function ResetTokenSuccess() {
  return { type: types.IDENTIFICATION_RESET_TOKEN_SUCCESS_USER };
}
export function ResetTokenError() {
  return { type: types.IDENTIFICATION_RESET_TOKEN_ERROR_USER };
}

export function newPasswordSucces(){
  return {
  type: types.NEW_PASSWORD_SUCCESS_USER };
}
export function newPasswordError(message){
  return { 
  type: types.NEW_PASSWORD_ERROR_USER,
  message 
  };
}

export function identificationResetToken(data) {
  return dispatch => {
    dispatch(beginIdenResetToken());
    return makeUserRequest('post', data, '/api/identificationResetToken')
      .then(response => {
        if (response.status === 200) {
          dispatch(ResetTokenSuccess(response.data));
        } else {
          dispatch(ResetTokenError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(ResetTokenError(getMessage(err)));
      });
  };
}

export function new_password(data) {
  return dispatch => {
    return makeUserRequest('post', data, '/api/new_password')
      .then(response => {
        if (response.status === 200) {
          dispatch(newPasswordSucces());
          dispatch(push('/login'));
        } else {
          dispatch(newPasswordError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(newPasswordError('Ошибка идентификации токена!'));
      });
  };
}

export function manualLogin(data) {
  return dispatch => {
    dispatch(beginLogin());

    return makeUserRequest('post', data, '/login')
      .then(response => {
        if (response.status === 200) {
          dispatch(loginSuccess(response.data));         
          dispatch(push('/'));
        } else {
          dispatch(loginError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(loginError(getMessage(err)));
      });
  };
}

export function password_reset(data) {
  return dispatch => {
    dispatch(beginPasswordReset());

    return makeUserRequest('post', data, '/api/password_reset')
      .then(response => {
        if (response.status === 200) {
          dispatch(passwordResetSuccess(response.data.message));
        } else {
          dispatch(passwordResetError('Oops! Something went wrong'));
        }
      })
      .catch(err => {
        dispatch(passwordResetError(getMessage(err)));
      });
  };
}

export function logOut() {
  return dispatch => {
    dispatch(beginLogout());

    return makeUserRequest('post', null, '/logout')
      .then(response => {
        if (response.status === 200) {
          dispatch(logoutSuccess());
          dispatch(push("/login"));
        } else {
          dispatch(logoutError());
        }
      });
  };
}
