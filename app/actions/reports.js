/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';
import {saveAs} from 'file-saver';
import { push } from 'react-router-redux';
polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeTestRequest(method, data, api = '/api/userslist', config) {
  return request[method](api, data, config);
}


function beginReports(){
  return{
    type: types.GET_REPORTS,
  };
}

function getReportsSuccess(data){
  return{
    type: types.GET_REPORTS_SUCCESS,
    data
  };
}

function getReportsError(message){
  return{
    type: types.GET_REPORTS_ERROR,
    message
  };
}

export function saveReports(type, testing_center, startDate, endDate, body) {
  return dispatch => {
    return makeTestRequest('get', {}, '/assets/styles/main.css')
      .then(response => {
        var fname="";
        if(type=="summary_table") fname+="Сводная_таблица_";
        else if(type=="testing_center") fname+="Аналитика_по_центрам_тестирования_";
        else if(type=="certificates") fname+="Реестр_выданных_сертификатов_"; else fname+='other_';
        fname+=startDate+'_'+endDate;
        var html=['<html><head><meta data-react-helmet="true" charset="utf-8"><style>'+response.data+'</style></head><body>'+body.innerHTML+'</body></html>'];
        var blob = new Blob(html, {type: "application/html;charset=utf-8"});
        saveAs(blob, fname+".html");        
      })
      .catch(err => {
        console.log('Ошибка загрузки стилей!');
      });

      /**/
  };
}

export function printReports() {
  return dispatch => {
      if (window.print) {
        window.print() ; 
      } else {
        var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
        document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
        WebBrowser1.ExecWB(6, 2);//Use a 1 vs. a 2 for a prompting dialog box WebBrowser1.outerHTML = ""; 
        }
  };
}

export function getReports(type, testing_center, startDate, endDate) {
  return dispatch => {
    dispatch(beginReports());
    var data={
      'type': type,
      'testing_center': testing_center,
      'startDate': startDate,
       'endDate':endDate
    }
    return makeTestRequest('post', data, '/api/reports/'+type)
      .then(response => {
        if (response.status === 200) {
          dispatch(getReportsSuccess(response.data.data));
        } else {
          dispatch(getReportsError('Oops! Something went wrong!'));
        }
      })
      .catch(err => {
        dispatch(getReportsError('Oops! Something went wrong!'));
      });
  };
}