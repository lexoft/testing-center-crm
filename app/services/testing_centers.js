import axios from 'axios';

const service = {
  getTesting_centers: () => axios.get('api/testing_centers')
};

export default service;
