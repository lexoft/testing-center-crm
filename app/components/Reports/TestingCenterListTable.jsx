import React, { PropTypes } from 'react';
import moment from 'moment';
import { Link } from 'react-router';

import '../../css/reports.css';
const TestingCenterListTable = ({ list, startDate, endDate, testing_centers}) => {

function testing_center_array(){
   var arr={};
  testing_centers.map(list => ( arr[list.id]=list.name));
  return arr;
}
var qualityType = testing_center_array();

function testing_centerFormatter(data) {
    return qualityType[data];
}
function percentFormatter(data) {
  return (
    <div>{+(data).toFixed(0)}</div>
  );
}
function dateFormatter(data) {
  var date = new Date(data);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  return (day+'.'+month+'.'+year);
}
function generateColumn(row){
  if(!row.testing_center){
    return(false);
  } else return(
          <tr>
            <td>{testing_centerFormatter(row.testing_center)}</td>
            <td>{row.city}</td>
            <td style={{"textAlign":"right"}}>{row.total}</td>
            <td style={{"textAlign":"right"}}>{row.issued}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.gpa)}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.the_first)}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.training)}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.age)}</td>
            <td style={{"textAlign":"right"}}>{row.men}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.sum_cost)}</td>
            <td style={{"textAlign":"right"}}>{percentFormatter(row.sum_discount)}</td>
          </tr>
          );
  }

return (
  <div>
  <h4>Аналитика по центрам тестирования</h4>
  <hr/>
  за период с {dateFormatter(new Date(+startDate))} по {dateFormatter(new Date(+endDate))}

<table className="reports-table">
  <thead>
    <tr>
        <th>Центр тестирования</th>
        <th>Страна</th>
        <th>Всего тестируемых за период</th>
        <th>Выдано сертификатов</th>
        <th>Средний балл результатов</th>
        <th>Доля сдавших с первого раза (%)</th>
        <th>Доля тестируемых прошедших обучение (%)</th>
        <th>Средний возраст тестируемых</th>
        <th>Кол-во мужчин</th>
        <th>Обороты, руб</th>
        <th>Сумма скидок, руб</th>
    </tr>
  </thead>
  <tbody>
      { list.length>0 ? list.map(lists => generateColumn(lists)) : <tr><td colSpan="11" style={{"textAlign":"center"}}>Нет данных</td></tr> }
  </tbody>
</table>
  </div>
);
};

TestingCenterListTable.propTypes = {
  list: PropTypes.array.isRequired,
};

export default TestingCenterListTable;
