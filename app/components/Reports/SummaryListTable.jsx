import React, { PropTypes } from 'react';
import moment from 'moment';
import { Link } from 'react-router';

import '../../css/reports.css';
const TestingCenterListTable = ({ list, startDate, endDate, testing_centers}) => {

function testing_center_array(){
   var arr={};
  testing_centers.map(list => ( arr[list.id]=list.name));
  return arr;
}
var qualityType = testing_center_array();

function testing_centerFormatter(data) {
    return qualityType[data];
}
function percentFormatter(data) {
  return (
    <div style={{"textAlign":"center"}}>{+((data)*100).toFixed(1)}</div>
  );
}
function dateFormatter(data) {
  var date = new Date(data);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  return (day+'.'+month+'.'+year);
}
function generateColumn(row, index){
        return(
          <tr>
            <td>{index+1}</td>
            <td>{row.name}<br/>{row.name_trans}</td>
            <td>{row.surname}<br/>{row.surname_trans}</td>
            <td>{row.citizenship}</td>
            <td>{dateFormatter(row.certification.test_date)}</td>
            <td>{percentFormatter(row.certification.result.reading/60)}</td>
            <td>{percentFormatter(row.certification.result.letter/40)}</td>
            <td>{percentFormatter(row.certification.result.grammar/50)}</td>
            <td>{percentFormatter(row.certification.result.listening/70)}</td>
            <td>{percentFormatter(row.certification.result.speaking/80)}</td>
            <td>{percentFormatter(row.certification.result.history/100)}</td>
            <td>{percentFormatter(row.certification.result.law/100)}</td>
            <td>{percentFormatter(row.certification.result.all/500)}</td>
            <td>Сертификат<br/>Комплексный экзамен для трудящихся мигрантов</td>
          </tr>
          );
  }

return (
  <div>
  <h4 className="summary-head">Сводная таблица результатов Регионального тестирования иностранных граждан на знание русского языка, истории России и основ законодательства РФ для получения разрешения на работу (патента) на территории Республики Башкортостан в <div className="signing-up"><p>(название организации)</p></div></h4>
<table className="reports-table">
  <thead>
    <tr>
        <th rowSpan="2">№<p style={{whiteSpace:"nowrap", fontWeight:"600"}}>п/п</p></th>
        <th rowSpan="2">Фамилия<p>русскими/латинскими</p></th>
        <th rowSpan="2">Имя<p>русскими/латинскими</p></th>
        <th rowSpan="2">Страна</th>
        <th rowSpan="2">Дата тестирования</th>
        <th colSpan="8">Результаты (%)</th>        
        <th rowSpan="2">Уровень</th>
    </tr>
        <tr>
            <th><p>1</p><p>Чтение</p><p>100%</p><p>60 б.</p></th>
            <th><p>2</p><p>Письмо</p><p>100%</p><p>40 б.</p></th>
            <th><p>3</p><p>Грамматика</p><p>100%</p><p>50 б.</p></th>
            <th><p>4</p><p>Аудирование</p><p>100%</p><p>70 б.</p></th>
            <th><p>5</p><p>Устная</p><p>100%</p><p>80 б.</p></th>
            <th><p>6</p><p>История</p><p>100%</p><p>100 б.</p></th>
            <th><p>7</p><p>Законодательство</p><p>100%</p><p>100 б.</p></th>
            <th><p>Общий балл %</p><p>100%</p><p>500 б.</p></th>
        </tr>        
  </thead>
  <tbody>
      { list.length>0 ? list.map((lists, index )=> generateColumn(lists, index)) : <tr><td colSpan="14" style={{"textAlign":"center"}}>Нет данных</td></tr> }
  </tbody>
</table>
<div className="reports-footer">
<h4>Ответственный за проведение тестирования:</h4>
<h4 style={{textAlign: "right"}}>(Ф.И.О., подпись)</h4>
<h4 style={{display: "block"}}>Тесторы-члены комиссии:</h4>
  <div>
    <h4>1. _______________________________________________</h4><h4 style={{textAlign: "left"}}>(Ф.И.О., подпись)</h4>
  </div>
  <div>
    <h4>2. _______________________________________________</h4><h4 style={{textAlign: "left"}}>(Ф.И.О., подпись)</h4>
  </div>
<h4 style={{display: "block"}}>Результаты тестирования проверены, аудиофайлы прослушаны.</h4>
<div>
    <h4>Председатель комиссии</h4><h4 style={{textAlign: "left", whiteSpace:"nowrap"}}>_______________________________________________(Ф.И.О., подпись)</h4>
</div>
</div>

  </div>
);
};
TestingCenterListTable.propTypes = {
  list: PropTypes.array.isRequired,
};

export default TestingCenterListTable;
