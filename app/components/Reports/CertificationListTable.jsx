import React, { PropTypes } from 'react';
import moment from 'moment';
import { Link } from 'react-router';

import '../../css/reports.css';
const CertificationListTable = ({ list, startDate, endDate}) => {

/*function fioFormatter(data) {
  return (
    <div><Link to={ '/tested/test/'+row._id }>{data}</Link></div>
  );
}*/


function dateFormatter(data) {
  var date = new Date(data);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  return (day+'.'+month+'.'+year);
}
function sexFormatter(data) {
  return (
    <div>{data ? "Муж.": "Жен."}</div>
  );
}
function trainingFormatter(data) {
  return (
    <div>{data ? "Проходил": "Не проходил"}</div>
  );
}
function firstFormatter(data) {
  return (
    <div>{data>0 ? "Две": "Одна"}</div>
  );
}


function generateColumn(row){
    return(
    <tr>
      <td>{dateFormatter(row.certification.date_of_issue)}</td>
      <td>{row.certification.certification_number}</td>
      <td>{row.fio}</td>
      <td>{row.citizenship}</td>
      <td>{row.phone}</td>
      <td>{sexFormatter(row.sex)}</td>
      <td>{dateFormatter(row.birthday)}</td>
      <td>{trainingFormatter(row.certification.training)}</td>
      <td>{firstFormatter(row.certification.discount)}</td>
      <td>{row.updateUsers && row.updateUsers.length > 0 ? row.updateUsers[row.updateUsers.length-1].name : 'Не указано'}</td>
    </tr>
    );
  }

return (
  <div>
  <h4>Реестр выданных сертификатов</h4>
  <hr/>
  за период с {dateFormatter(new Date(+startDate))} по {dateFormatter(new Date(+endDate))}

<table className="reports-table">
  <thead>
    <tr>
        <th>Дата выдачи</th>
        <th>Номер сертификата</th>
        <th>ФИО</th>
        <th>Гражданство</th>
        <th>Телефон</th>
        <th>Пол</th>
        <th>Дата рождения</th>
        <th>Обучение</th>
        <th>Попытки</th>
        <th>Сотрудник</th>
    </tr>
  </thead>
  <tbody>
    
      { list.length>0 ? list.map(lists => generateColumn(lists)) : <tr><td colSpan="10" style={{"textAlign": "center"}}>Нет данных</td></tr> }
  </tbody>
</table>
  </div>
);
};

CertificationListTable.propTypes = {
  list: PropTypes.array.isRequired,
};

export default CertificationListTable;
