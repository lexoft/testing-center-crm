import React, {  Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {ControlLabel, Col, FormControl, FieldGroup, FormGroup, Button} from 'react-bootstrap';
import {countries} from './countries.js';
import Select from 'react-select';
import '../../css/react-select.css';
import DatePicker from 'react-datepicker';
import '../../css/react-datepicker.css';
import moment from 'moment';

export default class Data extends Component {
constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.updateCenterValue = this.updateCenterValue.bind(this);
    this.updateCitizenshipValue = this.updateCitizenshipValue.bind(this);
    //this.handleChangeDate = this.handleChangeDate.bind(this);
    this.state={
    	selectCenter:'',
    	citizenship:'',
    	birthday:moment(),
    	date_of_issue:moment()
    };
    }
handleOnSubmit(event) {
    event.preventDefault();
    const { addData, id} = this.props;
    var _id="new";
    if(id!="new"){
    	_id=id;
    } 
    const sex = event.target.elements.sex.value;
    const testing_center = this.state.selectCenter;
    const surname = ReactDOM.findDOMNode(this.refs.surname).value;
    const name = ReactDOM.findDOMNode(this.refs.name).value;
    const surname_trans = ReactDOM.findDOMNode(this.refs.surname_trans).value;
    const name_trans = ReactDOM.findDOMNode(this.refs.name_trans).value;
    const birthday = this.dateFormatter(this.state.birthday);
    const citizenship = this.state.citizenship;
    const document_type = ReactDOM.findDOMNode(this.refs.document_type).value;
    const series = ReactDOM.findDOMNode(this.refs.series).value;
    const number = ReactDOM.findDOMNode(this.refs.number).value;
    const date_of_issue = this.dateFormatter(this.state.date_of_issue);
    const issued_by = ReactDOM.findDOMNode(this.refs.issued_by).value;
    const series_migration_cards = ReactDOM.findDOMNode(this.refs.series_migration_cards).value;
    const number_migration_cards = ReactDOM.findDOMNode(this.refs.number_migration_cards).value;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    const phone = ReactDOM.findDOMNode(this.refs.phone).value;
    addData({_id,"certification.testing_center":testing_center, surname, surname_trans, name, name_trans, birthday, citizenship, document_type, series, number, date_of_issue, issued_by, series_migration_cards, number_migration_cards, email, phone, sex });
    //onEntrySave({fio, email, password, testing_center, role});
  }
formaterDate(date){
	if(date!=""){
		var arr=date.split('-');
		return arr[2]+'.'+arr[1]+'.'+arr[0];
	} else return '';
}
formatterTesting_center(data){
	const { user } = this.props;
	var res;
	user['profile']['testing_center'].map(list => (list.id==data ? res=list.name : ""));
	return res;
}
updateCenterValue (newValue) {
    this.setState({
      selectCenter: newValue
    });
}
updateCitizenshipValue (newValue) {
    this.setState({
      citizenship: newValue
    });
}
componentDidMount(){
	const {test: {testid}, id, user} = this.props;
	if(id=="new"){
		this.setState({
			selectCenter: user.profile.testing_center[0].id,
			citizenship: countries[0].name
		});
	} else {
		this.setState({
			selectCenter: (typeof testid.certification === 'undefined') ? '' : testid.certification.testing_center,
			citizenship: testid.citizenship,
			birthday:moment(testid.birthday),
			date_of_issue:moment(testid.date_of_issue)
		});
	}
}
  handleChangeDate(type, date) {
    this.setState({
      [type] : date
    });
  }
dateFormatter(data) {
  var date = new Date(+data);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  return (year+'-'+month+'-'+day);
} 
render(){
	//console.log(this.props);
	const {test: {testid} , id, user}=this.props;
    moment.updateLocale('en', {
    months : [
        "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
        "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    ],
    weekdaysMin : ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    });	
	return (
		<Col className={user.profile.role != 0 && 'formadd'} sm={user.profile.role != 0 ? 12 : 6}>
	      <form onSubmit={this.handleOnSubmit}>
	      <Col sm={user.profile.role != 0 ? 5 : 12}>
			  {user.profile.role != 0 && <h2 className="head-text">Основные данные</h2>}
	          <FormGroup controlId="formControlsSurname">
	            <ControlLabel>Центр тестирования:</ControlLabel>
	            { user.profile.role!="0" ?
		              <Select 
		                options={user['profile']['testing_center']} 
		                labelKey="name" valueKey="id"
		                value={this.state.selectCenter}
		                onChange={this.updateCenterValue}
		                placeholder=''
		              	simpleValue  searchable={false} clearable={false}
		              />  
		        : <p>{ this.formatterTesting_center(testid.certification.testing_center) }</p>}
	          </FormGroup>
	          <FormGroup controlId="formControlsSurname">
	            <ControlLabel>Фамилия:</ControlLabel>
	            { user.profile.role!="0" ?
	              <FormControl type="text"
	              required
	                ref="surname"
	                defaultValue={id=="new" ? "" :testid.surname}
	                 />
	            : <p>{ testid.surname }</p> }
	          </FormGroup>
	          <FormGroup controlId="formControlsSurname">
	            <ControlLabel>Фамилия(латиницей):</ControlLabel>
	            { user.profile.role!="0" ?
	              <FormControl type="text"
	                ref="surname_trans"
	                defaultValue={id=="new" ? "" :testid.surname_trans}
	                 />
	            : <p>{ testid.surname_trans }</p>}
	          </FormGroup>	          
	          <FormGroup controlId="formControlsName">
	            <ControlLabel>Имя:</ControlLabel>
	            { user.profile.role!="0" ?
	              <FormControl type="text"
	              required
	                ref="name"
	                defaultValue={id=="new" ? "" :testid.name}
	                 />
	            :  <p>{  testid.name }</p> }
	          </FormGroup>
	          <FormGroup controlId="formControlsName">
	            <ControlLabel>Имя(латиницей):</ControlLabel>
		            { user.profile.role!="0" ?
		              <FormControl type="text"
		                ref="name_trans"
		                defaultValue={id=="new" ? "" :testid.name_trans}
		                 />
		            :<p>{ testid.name_trans }</p>}
	          </FormGroup>
              { user.profile.role != "0" &&
			  <div>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Пол:</ControlLabel>
					  <div className="radio">
						  <label className="radio-inline">
							  <input type="radio" name="sex" value="true" defaultChecked={testid.sex}/> Муж
						  </label>
						  <label className="radio-inline">
							  <input type="radio" name="sex" value="false" defaultChecked={!testid.sex}/> Жен
						  </label>
					  </div>
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Дата рождения:</ControlLabel>
						  <div>
							  <DatePicker
								  showYearDropdown
								  dateFormat="DD.MM.YYYY"
								  selected={this.state.birthday}
								  onChange={this.handleChangeDate.bind(this, 'birthday')}
								  className="form-control"
							  />
						  </div>
				  </FormGroup>
			  </div>
              }
				<FormGroup controlId="formControlsPatronymic">
					<ControlLabel>Гражданство:</ControlLabel>
					{ user.profile.role!="0" ?
		              <Select 
		              	noResultsText="Нет данных"
		                options={countries} 
		                labelKey="name" valueKey="name"
		                value={this.state.citizenship}
		                onChange={this.updateCitizenshipValue}
		                placeholder=''
		              	simpleValue  searchable={true} clearable={false}
		              />  					
			        : <p>{ testid.citizenship }</p> }
				</FormGroup>
              { user.profile.role!="0" &&
			  <div>
				  <h2>Паспортные данные</h2>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Вид документа:</ControlLabel>
					  <FormControl type="text"
								   ref="document_type"
								   defaultValue={id=="new" ? "" :testid.document_type}
					  />
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Серия:</ControlLabel>
					  <FormControl type="text"
								   ref="series"
								   defaultValue={id=="new" ? "" :testid.series}
					  />
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Номер:</ControlLabel>
					  <FormControl type="text"
								   ref="number"
								   defaultValue={id=="new" ? "" :testid.number}
					  />
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Дата выдачи:</ControlLabel>
					  <DatePicker
						  showYearDropdown
						  dateFormat="DD.MM.YYYY"
						  selected={this.state.date_of_issue}
						  onChange={this.handleChangeDate.bind(this, 'date_of_issue')}
						  className="form-control"
					  />
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Кем выдан:</ControlLabel>
					  <FormControl type="text"
								   ref="issued_by"
								   defaultValue={id=="new" ? "" :testid.issued_by}
					  />
				  </FormGroup>
			  </div>
              }
				<FormGroup controlId="formControlsPatronymic">
					<ControlLabel>Серия миграционной карты:</ControlLabel>
				{ user.profile.role!="0" ?
					<FormControl type="text"
					ref="series_migration_cards"
					defaultValue={id=="new" ? "" :testid.series_migration_cards}
					/>    
				: <p>{ testid.series_migration_cards }</p> }
				</FormGroup>
				<FormGroup controlId="formControlsPatronymic">
					<ControlLabel>Номер миграционной карты:</ControlLabel>
				{ user.profile.role!="0" ?
					<FormControl type="text"
					ref="number_migration_cards"
					defaultValue={id=="new" ? "" :id=="new" ? "" :testid.number_migration_cards}
					/>
				: <p>{ testid.number_migration_cards }</p> }
				</FormGroup>
		{ user.profile.role!="0" ?
	      <Button  className="btn-primary" type="submit"
	      >
	       { id=="new" ? "Добавить" : "Сохранить" }
	      </Button>
	    : ""}
		  </Col>
              { user.profile.role != "0" &&
			  <Col className="formadd" sm={5}>
				  <h2 className="head-text">Контактная информация</h2>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Email:</ControlLabel>
                      { user.profile.role != "0" ?
						  <FormControl type="text"
									   ref="email"
									   defaultValue={id == "new" ? "" : testid.email}
						  />
                          : <p>{ testid.email }</p> }
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Номер телефона:</ControlLabel>
                      { user.profile.role != "0" ?
						  <FormControl type="text"
									   ref="phone"
									   defaultValue={id == "new" ? "" : testid.phone}
						  />
                          : <p>{ testid.phone }</p> }
				  </FormGroup>
			  </Col>
              }
		  </form>
	    </Col>
	);
}
}
Data.propTypes = {

	addData: PropTypes.func.isRequired,
};