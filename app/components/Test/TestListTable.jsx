import React, { PropTypes } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Link } from 'react-router';
import '../../css/react-bootstrap-table.css';
import {Form, FormControl, FieldGroup, FormGroup, Button, ButtonToolbar, ButtonGroup, Col} from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import '../../css/testlist.css';
import Select from 'react-select';
import '../../css/react-select.css';
const TestListTable = ({ testlist, testing_center, onPageChange, totalSize, page, pageSize, onSizePerPageList, inputChange, selectChange, updateDaysChange, days, onSortChange, user, selectCT, totalTests, totalIssued}) => {

function testing_center_array(){
   var arr={};
  testing_center.map(list => ( arr[list.id]=list.name));
  return arr;
}
var qualityType = testing_center_array();

function statusFormatter(cell, row) {
  return (
    <div>
    {cell=="Новый" ? <span className="label label-info">Новый</span>: ""}
    {cell=="Оплачен" ? <span className="label label-primary">Оплачен</span>: ""}
    {cell=="Тестирование пройдено" ? <span className="label label-success">Тестирование пройдено</span>: ""}
    {cell=="Сертификат выдан" ? <span className="label label-warning">Сертификат выдан</span>: ""}
    {cell=="Недействителен" ? <span className="label label-danger">Недействителен</span>: ""}
    </div>
  );
}

function fioFormatter(cell, row) {
  return (
    <div><Link to={ '/tested/test/'+row._id }>{cell}</Link></div>
  );
}

function certificationFormatter(cell, row) {
  return (
  <div>
    <div>{cell}</div>
    <div>Стоимость: {row.cost_test-row.discount}</div>
  </div>
  );
}

function dateFormatter(cell, row) {
  var date = new Date(cell);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  if (year === 1970) {
      return '';
  }
  return (
    <div>{day+'.'+month+'.'+year}</div>
    //<div></div>
  );
}

function citizenshipFormatter(cell, row) {
  return (
    <div>{cell}</div>
  );
}

function idFormatter(cell, row) {
    if(cell>0) {
        return (
            <div style={{textAlign: "center"}}>{("00000000000" + cell).substring(cell.length)}</div>
        );
    } else {
        return(<div></div>);
    }
}

function createCustomButtonGroup(props){
    return (
    <ButtonToolbar>
    { user.profile.role!="0" ?
      <ButtonGroup>
          <LinkContainer to="/tested/test/new">
          <Button
            bsStyle="primary"
            >
            Добавить тестируемого
          </Button>     
          </LinkContainer>
      </ButtonGroup>
    : "" }
      <ButtonGroup onSelect={ updateDaysChange }>
        <Button onClick={updateDaysChange} active={ days=="today" ? true : false} value="today">Сегодня</Button>
        <Button onClick={updateDaysChange} active={ days=="tomorrow" ? true : false} value="tomorrow">Завтра</Button>
        <Button onClick={updateDaysChange} active={ days=="week" ? true : false} value="week">На этой неделе</Button>
        <Button onClick={updateDaysChange} active={ days=="all" ? true : false} value="all">Все</Button>
      </ButtonGroup>
    </ButtonToolbar>
    );
  }

 function MySearchPanel(props){
  var opt=[{'id':'', 'name':'Все'}];
  opt=opt.concat(testing_center);
    return (
      <Col md={12}>
            <Col md={6} style={{paddingLeft:0, paddingRight:0}}>
                  <Select 
                    options={opt} 
                    labelKey="name" valueKey="id"
                    value={selectCT}
                    onChange={selectChange}
                    placeholder=''
                    simpleValue  searchable={false} clearable={false}
                  />  
            </Col>
            <Col md={6} style={{paddingLeft:0, paddingRight:0}}>      
            <FormControl type="text"
              ref="email"
              placeholder="Поиск..."
              onChange={inputChange}
              style={{'padding-right':'14px'}}
            />
            </Col>
      </Col>
    );
  }
function columnClassNameFormat(fieldValue, row, rowIdx, colIdx) {
  return rowIdx % 2 === 0 ? '' : 'tr-function-example';
}
    function testing_centerFormatter(cell, row, enumObject) {
        return enumObject[cell];
    }

function columnClassNameStatus(fieldValue, row, rowIdx, colIdx) {
  if(fieldValue=="Новый") return 'td-function-new';
  if(fieldValue=="Оплачен") return 'td-function-paid';
  if(fieldValue=="Тестирование пройдено") return 'td-function-travel';
  if(fieldValue=="Сертификат выдан") return 'td-function-issued';
  if(fieldValue=="Недействителен") return 'td-function-notvalid';
}
    function renderPaginationPanel (props) {
        return (
            <div className="col-lg-12 col-md-12 col-xs-12" style={{ padding: 0 }}>
                <div className="col-lg-4 col-md-4 col-xs-4" style={{ padding: 0 }}>
                    { props.components.sizePerPageDropdown }
                    <div  style={{ display: 'inline-block', paddingLeft: '15px' }}>
                        <h5 style={{ margin: 0 }}>Всего тестируемых {totalTests}</h5>
                        <h5 style={{ margin: 0 }}>Всего выдано сертификатов {totalIssued}</h5>
                    </div>
                </div>
                <div className="col-lg-8 col-md-8 col-xs-8" style={{ padding: 0 }}>{ props.components.pageList }</div>
            </div>
        );
    };
    const options = {
      page: page,
      sizePerPage: pageSize,
      sizePerPageList: [ 10, 20 ], 
      onPageChange: onPageChange,
      onSizePerPageList: onSizePerPageList,
      searchPanel: MySearchPanel,
      btnGroup: createCustomButtonGroup,
      noDataText: 'Нет данных',
      onSortChange: onSortChange,
      paginationPanel: renderPaginationPanel
      //afterSearch: afterSearch  // define a after search hook
    };
  return (
      <BootstrapTable data={ testlist }
      fetchInfo={ { dataTotalSize: totalSize } }
      options={options}
      remote
      pagination
      search
      >
        <TableHeaderColumn dataSort={ true } searchable={ false } columnClassName={ columnClassNameFormat } isKey={true}  dataField='status' width='220' dataFormat={ statusFormatter }>Статус</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } dataField='fio' columnClassName={ columnClassNameFormat } dataFormat={ fioFormatter } headerAlign='center' >ФИО</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } dataField='registration_number' columnClassName={ columnClassNameFormat } dataFormat={ idFormatter } headerAlign='center' >Регистрационный номер</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } dataField='certification_number' columnClassName={ columnClassNameFormat } headerAlign='center' >Номер сертификата</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } searchable={ false } columnClassName={ columnClassNameFormat } dataField='type_of_test' dataFormat={ certificationFormatter } headerAlign='center'>Тестирования</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } searchable={ false } columnClassName={ columnClassNameFormat } dataField='test_date' dataFormat={dateFormatter} width='170'   headerAlign='center'>Дата тестирования</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } searchable={ false } columnClassName={ columnClassNameFormat } dataField='testing_center' dataFormat={testing_centerFormatter} formatExtraData={ qualityType } headerAlign='center'>Центр тестирования</TableHeaderColumn>
        <TableHeaderColumn dataSort={ true } searchable={ false } columnClassName={ columnClassNameFormat } dataField='citizenship'  width='150' headerAlign='center'>Гражданство</TableHeaderColumn>
      </BootstrapTable>
  );
};

TestListTable.propTypes = {
  testlist: PropTypes.array.isRequired,
  testing_center: PropTypes.array.isRequired,
  onPageChange: PropTypes.func.isRequired,
  //sizePerPageListChange: PropTypes.func.isRequired,
};

export default TestListTable;
