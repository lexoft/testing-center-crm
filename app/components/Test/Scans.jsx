 import React, {  Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {Button , FormControl, FormGroup, Col, ProgressBar} from 'react-bootstrap';
import axios from 'axios';
//import { NavStep } from '../../actions/test';
import '../../css/scans.css';
export default class Scans extends Component {
constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.deleteOnSubmit = this.deleteOnSubmit.bind(this);
    this.state = {
        load:0
    };
    }
handleOnSubmit(event) {
	event.preventDefault();
    var component = this;
	    const {id, getTestById, addTestScansSuccess, addTestScansError} = this.props;
        let formData = new FormData();
        formData.append("id", id);
        formData.append("file", ReactDOM.findDOMNode(this.refs.file).files[0]);
        const config = {
            headers: { 'content-type': 'multipart/form-data' },
            onUploadProgress: function (progressEvent) {
                //console.log((progressEvent.loaded/progressEvent.total)*100);((this.state.speaking/80)*100).toFixed(1)
                component.setState({ load: +((progressEvent.loaded/progressEvent.total)*100).toFixed(0) });
              },
        }
        const url = '/api/uploadfile';
        axios.post(url, formData, config)
            .then(function(response) {
                addTestScansSuccess(response.data.message);
                getTestById({'id': id}, false);
                component.setState({ load: 0 });
                ReactDOM.findDOMNode(component.refs.file).value="";
            })
            .catch(function(error) {
                if(error.response.status=="413"){
                    alertify.error('Слишком большой размер файла, максимальный размер: 250MB');
                } else {
                    alertify.error('Для загрузки доступны документы, видео, изображения, аудиофайлы, архивы');
                }
                component.setState({ load: 0 });
                ReactDOM.findDOMNode(component.refs.file).value="";
            }); 
}
deleteOnSubmit(scans_id, fname){
    const {deleteTestScans, id, user}=this.props;
    deleteTestScans({'_id':id, 'scans_id': scans_id, 'fname': fname});
}
render(){
    const {test, user} = this.props;
    return (
        <Col sm={12}>
        <h2 className="head-text">Вложения</h2>
        <ul className="list-group">
        {test.testid["scans"].map(list =>  <li className="list-group-item"><Link to={"/scans/"+list._id+"_"+list.name} target="_blank">{list.name}</Link> { user.profile.role!="0" ? <Link onClick={() =>this.deleteOnSubmit(list._id, list.name)}><span className="glyphicon glyphicon-remove icon-remove"></span></Link> : "" }</li>)}
        </ul>
        { user.profile.role!="0" ?
          <form onSubmit={this.handleOnSubmit}>

          <FormGroup controlId="formControlsPatronymic">
	              <FormControl type="file"
	              required
	                ref="file"
	                 />	               
            </FormGroup
            >
            {this.state.load=="0" ? "" : <ProgressBar now={this.state.load} label={`${this.state.load}%`}/> }
            {this.state.load=="0" ? <Button className="btn-primary" type="submit">Загрузить</Button> : <Button className="btn-primary" type="submit" disabled>Загрузить</Button>}
          </form>
        : "" }
        </Col>
    );
}
}

Scans.propTypes = {
    uploadFile: PropTypes.func.isRequired,
    addTestScansSuccess: PropTypes.func.isRequired,
    addTestScansError: PropTypes.func.isRequired,
 	deleteTestScans: PropTypes.func.isRequired,

};