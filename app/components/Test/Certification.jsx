import React, {  Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {ControlLabel, Col, FormControl, FieldGroup, FormGroup, Button,  ButtonGroup, Radio, Table, thead, tbody, Alert} from 'react-bootstrap';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem  from 'react-bootstrap/lib/NavItem';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import Navbar from 'react-bootstrap/lib/Navbar';
import axios from 'axios';
import '../../css/certification.css';
import Select from 'react-select';
import '../../css/react-select.css';
import DatePicker from 'react-datepicker';
import '../../css/react-datepicker.css';
import moment from 'moment';

class Certification extends Component {
constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleSelectStatus = this.handleSelectStatus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCostChange = this.handleCostChange.bind(this);
    this.clickPDF = this.clickPDF.bind(this);
    this.updateTypeTestValue = this.updateTypeTestValue.bind(this);
    this.state = {
      cost_test: 0,
      discount: 0,
      selectStatus:"Новый",
      reading: 0,
      letter: 0,
      grammar: 0,
      listening: 0,
      speaking: 0,
      history: 0,
      law: 0,
      all: 0,
      type_of_test: '',
	  test_date: moment(),
      date_of_issue: moment()      
    };    
    }
handleOnSubmit(event) {
	event.preventDefault();
	var noerr=true;
	const { updateCertification, id, test:{testid}, testingcenterId} = this.props;
	const training = event.target.elements.training.value;
    const test_date = this.dateFormatter(this.state.test_date);
    const type_of_test = this.state.type_of_test;
    const cost_test = ReactDOM.findDOMNode(this.refs.cost_test).value;
    const discount = ReactDOM.findDOMNode(this.refs.discount).value;
    const status = this.state.selectStatus;
    if('Сертификат выдан'==this.state.selectStatus){
    	if(testingcenterId.threshold<=(this.state.reading+this.state.letter+this.state.grammar+this.state.listening+this.state.speaking+this.state.history+this.state.law)){
	        var certification_number = ReactDOM.findDOMNode(this.refs.certification_number).value;
	        var date_of_issue = this.dateFormatter(this.state.date_of_issue);
	        var comment = ReactDOM.findDOMNode(this.refs.comment).value;
    	} else{
    		alertify.error('Не достаточно баллов для выдачи сертификата');
    		noerr=false;
    	}
    } else {
        var certification_number = testid.certification.certification_number;
        var date_of_issue = testid.certification.date_of_issue;
        var comment = testid.certification.comment;
    }
    if(noerr){
        updateCertification({id, test_date, type_of_test, cost_test, discount,  certification_number, date_of_issue, comment, training, status,
        	"result":{
        		reading: this.state.reading,
    			letter: this.state.letter,
    			grammar: this.state.grammar,
    			listening: this.state.listening,
    			speaking: this.state.speaking,
    			history: this.state.history,
    			law: this.state.law,
    			all: this.state.all,
        	}
        });
    }
}
handleSelectStatus(data){
	const { testingcenterId }=this.props;
	if((data=='Сертификат выдан') &&  (testingcenterId.threshold>(this.state.reading+this.state.letter+this.state.grammar+this.state.listening+this.state.speaking+this.state.history+this.state.law))){
		alertify.error('Не достаточно баллов для выдачи сертификата');
	} else{
	this.setState({ selectStatus: data });
	}
}
formaterDate(date){
	if(date!=""){
		var arr=date.split('-');
		return arr[2]+'.'+arr[1]+'.'+arr[0];
	} else return '';
}
componentDidMount(){
	const {test:{testid}, getTestingCenterId}=this.props;
	this.setState({ 
		cost_test: testid.certification.cost_test,
		discount: testid.certification.discount,
		selectStatus: testid.certification.status,
		reading: testid.certification.result.reading,
		letter: testid.certification.result.letter,
		grammar: testid.certification.result.grammar,
		listening: testid.certification.result.listening,
		speaking: testid.certification.result.speaking,
		history: testid.certification.result.history,
		law: testid.certification.result.law,
		type_of_test: testid.certification.type_of_test,
		test_date: moment(testid.certification.test_date),
		date_of_issue: moment(testid.certification.date_of_issue),
		 });
	if(testid.certification.test_date=="") this.setState({test_date:moment()});
	if(testid.certification.date_of_issue=="") this.setState({date_of_issue:moment()});
	getTestingCenterId(testid.certification.testing_center);
}
  handleChange(event) {
  	event.preventDefault();
  	var value=Number(event.target.value);
  	if((event.target.name=="reading") && (value>60)) value=60;
  	if((event.target.name=="letter") && (value>40)) value=40;
  	if((event.target.name=="grammar") && (value>50)) value=50;
  	if((event.target.name=="listening") && (value>70)) value=70;
  	if((event.target.name=="speaking") && (value>80)) value=80;
  	if((event.target.name=="history") && (value>100)) value=100;
  	if((event.target.name=="law") && (value>100)) value=100;
  	if(value<0) value=0;
  	this.setState({[event.target.name]: value});
  	//this.setState({all: );
  }    
  clickPDF() {
  	const {generatePDF, id, test:{testid}} = this.props;
  	generatePDF(id, testid.registration_number);
  }  
 handleCostChange(event) {
  	event.preventDefault();
  	var value=Number(event.target.value);
  	if(event.target.name=="discount" && this.state.cost_test<value){
  		this.setState({cost_test: value});
  	}
  	if(event.target.name=="cost_test" && this.state.discount>value){
  		this.setState({discount: value});
  	}
  	if(value<0) value=0;
  	this.setState({[event.target.name]: value});
  	//this.setState({all: );
  }
	updateTypeTestValue (newValue) {
	    this.setState({
	      type_of_test: newValue
	    });
	}
  handleChangeDate(type, date) {
    this.setState({
      [type] : date
    });
  }
  statusFormatter(status) {
    return (
		<div>
            {status=="Новый" ? <span className="label label-info">Новый</span>: ""}
            {status=="Оплачен" ? <span className="label label-primary">Оплачен</span>: ""}
            {status=="Тестирование пройдено" ? <span className="label label-success">Тестирование пройдено</span>: ""}
            {status=="Сертификат выдан" ? <span className="label label-warning">Сертификат выдан</span>: ""}
            {status=="Недействителен" ? <span className="label label-danger">Недействителен</span>: ""}
		</div>
    );
}
dateFormatter(data) {
  var date = new Date(+data);
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  if(day<10) day='0'+day;
  if(month<10) month='0'+month;
  return (year+'-'+month+'-'+day);
} 	
render(){
const {test, test: { testid }, id, user, testspecies:{specieslist}, testingcenterId}=this.props;
    return (
	    <Col sm={user.profile.role != 0 ? 12 : 6}>
	      <Col className={user.profile.role != 0 && 'formadd'} sm={user.profile.role != 0 ? 8 : 12}>
	      <form onSubmit={this.handleOnSubmit}>
			  { user.profile.role === 0 ? <div>{this.statusFormatter(this.state.selectStatus)}</div>: <h2 className="head-text">Основные данные</h2>}
	          <FormGroup controlId="formControlsName">
	            <ControlLabel>Дата тестирования:</ControlLabel>
	            { user.profile.role!="0" ?
						<DatePicker
							showYearDropdown
						dateFormat="DD.MM.YYYY"
						selected={this.state.test_date}
						onChange={this.handleChangeDate.bind(this, 'test_date')}
						className="form-control"
						/>		                 
	            : <p>{this.formaterDate(testid.certification.test_date)}</p> }
	          </FormGroup>
	          <FormGroup controlId="formControlsPatronymic">
	            <ControlLabel>Вид тестирования:</ControlLabel>
	            { user.profile.role!="0" ?
		              <Select 
		                options={specieslist} 
		                labelKey="name" valueKey="name"
		                value={this.state.type_of_test}
		                onChange={this.updateTypeTestValue}
		                placeholder=''
		              	simpleValue  searchable={true} clearable={false}
		              	required
		              />
				: <p>{testid.certification.type_of_test}</p>}
	          </FormGroup>
              { user.profile.role != "0" &&
			  <div>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Стоимость тестирования, руб:</ControlLabel>
                      { user.profile.role != "0" ?
						  <FormControl type="number"
									   required
									   value={this.state.cost_test}
									   onChange={this.handleCostChange}
									   name="cost_test"
									   ref="cost_test"
									   min="0"
						  />
                          : <p>{testid.certification.cost_test}</p>}

				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Скидка, руб:</ControlLabel>
					  <ControlLabel className="to-pay">К оплате: {this.state.cost_test - this.state.discount}
						  руб</ControlLabel>
                      { user.profile.role != "0" ?
						  <FormControl type="number"
									   value={this.state.discount}
									   onChange={this.handleCostChange}
									   name="discount"
									   ref="discount"
									   min="0"
						  />
                          : <p>{testid.certification.discount}</p>}
				  </FormGroup>
				  <FormGroup controlId="formControlsPatronymic">
					  <ControlLabel>Обучение:</ControlLabel>
					  <div className="radio">
						  <label className="radio-inline">
							  <input type="radio" name="training" value="true"
									 defaultChecked={testid.certification.training}/> Прошел
						  </label>
						  <label className="radio-inline">
							  <input type="radio" name="training" value="false"
									 defaultChecked={!testid.certification.training}/> Не проходил
						  </label>
					  </div>
				  </FormGroup>
                  {['Тестирование пройдено', 'Сертификат выдан'].indexOf(this.state.selectStatus) != -1 ?
					  <div>
                          <div>
                              <h2 style={{ display: 'inline-block' }}>Результаты</h2>
                              <div className="to-pay" style={{ paddingTop: '40px' }}>
                                  { testingcenterId.threshold <= (this.state.reading + this.state.letter + this.state.grammar + this.state.listening + this.state.speaking + this.state.history + this.state.law) ?
                                      'Экзамен успешно сдан(' + testingcenterId.threshold + ')'
                                      :
                                      <div style={{ color: 'red', display: 'inline-block' }}>Слишком низкий итоговый балл({testingcenterId.threshold})</div>
                                  }
                              </div>
                          </div>
                          <Table className="result-table" responsive bordered condensed striped>
							  <thead>
							  <tr>
								  <th><p>Чтение</p>60</th>
								  <th><p>Письмо</p>40</th>
								  <th><p>Граммат</p>50</th>
								  <th><p>Аудиров</p>70</th>
								  <th><p>Устная</p>80</th>
								  <th><p>История</p>100</th>
								  <th><p>Закон</p>100</th>
								  <th><p>Общий</p>500</th>
							  </tr>
							  </thead>
							  <tbody>
							  <tr>
								  <td><FormControl type="number" value={this.state.reading} onChange={this.handleChange}
												   name="reading"/></td>
								  <td><FormControl type="number" value={this.state.letter} onChange={this.handleChange}
												   name="letter"/></td>
								  <td><FormControl type="number" value={this.state.grammar} onChange={this.handleChange}
												   name="grammar"/></td>
								  <td><FormControl type="number" value={this.state.listening}
												   onChange={this.handleChange} name="listening"/></td>
								  <td><FormControl type="number" value={this.state.speaking}
												   onChange={this.handleChange} name="speaking"/></td>
								  <td><FormControl type="number" value={this.state.history} onChange={this.handleChange}
												   name="history"/></td>
								  <td><FormControl type="number" value={this.state.law} onChange={this.handleChange}
												   name="law"/></td>
								  <td><FormControl type="number" readonly
												   value={this.state.reading + this.state.letter + this.state.grammar + this.state.listening + this.state.speaking + this.state.history + this.state.law}
												   onChange={this.handleChange} name="all"/></td>
							  </tr>
							  <tr>
								  <td>{+((this.state.reading / 60) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.letter / 40) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.grammar / 50) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.listening / 70) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.speaking / 80) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.history / 100) * 100).toFixed(1)}%</td>
								  <td>{+((this.state.law / 100) * 100).toFixed(1)}%</td>
								  <td>{+(((this.state.reading + this.state.letter + this.state.grammar + this.state.listening + this.state.speaking + this.state.history + this.state.law) / 500) * 100).toFixed(2)}%</td>
							  </tr>
							  </tbody>
						  </Table>
					  </div>
                      : ""}
			  </div>
              }
				{(this.state.selectStatus=='Сертификат выдан') ?
					<div>
					<h2 className="head_certification">Сертификат</h2>
					{(testingcenterId.threshold<=(this.state.reading+this.state.letter+this.state.grammar+this.state.listening+this.state.speaking+this.state.history+this.state.law)) ?
					(<div>
					 						<FormGroup controlId="formControlsPatronymic">
					 							<ControlLabel>Номер сертификата:</ControlLabel>
					 							{ user.profile.role!="0" ?
					 								<FormControl type="text"
					 								required
					 								defaultValue={testid.certification.certification_number}
					 								ref="certification_number"
					 								/>
					 							: <p>{ testid.certification.certification_number }</p>}
					 						</FormGroup>
					 						<FormGroup controlId="formControlsPatronymic">
					 							<ControlLabel>Дата выдачи:</ControlLabel>
					 							{ user.profile.role!="0" ?
					 								<DatePicker
														showYearDropdown
													dateFormat="DD.MM.YYYY"
													selected={this.state.date_of_issue}
													onChange={this.handleChangeDate.bind(this, 'date_of_issue')}
													className="form-control"
													/>	
					 							: <p>{this.formaterDate(testid.certification.date_of_issue)}</p> }
					 						</FormGroup>
					 						<FormGroup controlId="formControlsPatronymic">
					 							<ControlLabel>Комментарий:</ControlLabel>
					 							{ user.profile.role!="0" ?
					 							<textarea className="form-control" rows="3" ref="comment" defaultValue={testid.certification.comment}></textarea>
					 							: <p>{testid.certification.comment}</p> }
					 						</FormGroup>
					 						</div>)
					: (<Alert bsStyle="danger"><h4>Недостаточно баллов для выдачи сертификата</h4></Alert>) }
					</div>
				: ""}
		{ user.profile.role!="0" ?
	      <Button className="btn-primary" type="submit"
	      >
	       Сохранить
	      </Button>
	    : ""}
	      </form>	      
	      </Col>
            { user.profile.role != "0" &&
			<Col className="formadd" smPush={1} sm={3}>
				<div className="panel panel-success">
					<div className="panel-heading"><h3>Статус</h3></div>
					<div className="panel-body">
						<Nav bsStyle="pills" className="status_pills" stacked activeKey={this.state.selectStatus}
							 onSelect={ this.handleSelectStatus }>
							<NavItem className="new" eventKey="Новый">Новый</NavItem>
							<NavItem className="paid" eventKey="Оплачен">Оплачен</NavItem>
							<NavItem className="travel" eventKey="Тестирование пройдено">Тестирование пройдено</NavItem>
							<NavItem className="issued" eventKey="Сертификат выдан">Сертификат выдан</NavItem>
							<NavItem className="notvalid" eventKey="Недействителен">Недействителен</NavItem>
						</Nav>
					</div>
				</div>
                { testid.certification.status == 'Сертификат выдан' ?
					<Button className="btn-primary btn-pdf" type="submit" onClick={this.clickPDF}
							disabled={test.pdfloading}>{ test.pdfloading ? "Загрузка..." : "Сгенерировать документ" }</Button>
                    : ""}
			</Col>
            }
	    </Col>
    );
}
}

Certification.propTypes = {
	updateCertification: PropTypes.func.isRequired,
	//selectStatus: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
  	user: state.user,
  	testspecies: state.testspecies,
  	testingcenterId: state.testingcenters.testingCenterId,
  };
}

export default connect(mapStateToProps)(Certification);
