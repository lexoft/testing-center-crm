import React, { Component, PropTypes } from 'react';


import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import ReactDOM from 'react-dom';
import Button from 'react-bootstrap/lib/Button';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Col from 'react-bootstrap/lib/Col';

export default class TestingCentersAddForm extends Component {
  constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);    
    }
  handleOnSubmit(event) {
    event.preventDefault();
    const full_name = ReactDOM.findDOMNode(this.refs.full_name).value;
    const short_name = ReactDOM.findDOMNode(this.refs.short_name).value;
    const head = ReactDOM.findDOMNode(this.refs.head).value;
    const contact = ReactDOM.findDOMNode(this.refs.contact).value;
    const adress = ReactDOM.findDOMNode(this.refs.adress).value;
    var threshold = ReactDOM.findDOMNode(this.refs.threshold).value;
    var position = ReactDOM.findDOMNode(this.refs.position).value;
    if(threshold>500) threshold=500; 
    const { onEntrySave } = this.props;
    onEntrySave({full_name, short_name, head, contact, adress, threshold, position});
  }
  handleChange(event) {
    event.preventDefault();
    if(event.target.value>500) event.target.value=500;
    else if(event.target.value<0) event.target.value=0;
    else event.target.value=Number(event.target.value);
  }  
render(){
  return (
    <Col className="formadd" sm={6}>
      <form onSubmit={this.handleOnSubmit}>
          <FormGroup>
            <ControlLabel>Краткое наименование центра:</ControlLabel>
              <FormControl type="mail"
                ref="short_name"
                required
                 />    
          </FormGroup>      
          <FormGroup>
            <ControlLabel>Полное наименование центра(в тв. падеже):</ControlLabel>
            <textarea className="form-control" rows="3" ref="full_name" required></textarea>
          </FormGroup> 
          <FormGroup>
            <ControlLabel>Руководитель центра:</ControlLabel>
              <FormControl type="mail"
              required
                ref="head"
                 />    
          </FormGroup>           
          <FormGroup>
            <ControlLabel>Должность:</ControlLabel>
              <FormControl type="mail"
              required
                ref="position"
                 />    
          </FormGroup>  
          <FormGroup>
            <ControlLabel>Адрес центра:</ControlLabel>
              <FormControl type="mail"
                ref="adress"
                 />    
          </FormGroup>  
          <FormGroup>          
            <ControlLabel>Контактные данные центра:</ControlLabel>
              <FormControl type="mail"
                ref="contact"
                 />    
          </FormGroup>
          <FormGroup>          
            <ControlLabel>Пороговый балл для выдачи сертификата:</ControlLabel>
              <FormControl type="number" min="0" max="500"
                ref="threshold"
                onChange={this.handleChange}
                required
                 />    
          </FormGroup>                                                
      <Button className="btn-primary btn-lg" type="submit"
      >
       Добавить
      </Button>
      </form>
      </Col>
  );
}
};

TestingCentersAddForm.propTypes = {
  onEntrySave: PropTypes.func,
};
