import React, { PropTypes } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Link } from 'react-router';
import {Form, FormControl, FieldGroup, FormGroup, Button} from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import '../../css/react-bootstrap-table.css';


const UsersListTable = ({ userslist , addUserMode}) => {
function roleFormatter(cell, row) {
  return (
    <div>{cell == 0 ? "Наблюдатель": (cell == 1 ? "Менеджер": (cell == 2 ? "Руководитель":(cell == 3 ? "Администратор":"")))}</div>
  );
}
var roleType = {
    0: "Наблюдатель",
    1: "Менеджер",
    2: "Руководитель",
    3: "Администратор"
};
function centerFormatter(cell, row) {
  return (
    <div>{ cell ? cell.map(list => <div>{ list.name }</div>) : ""}</div>
  );
}
function usernameFormatter(cell, row) {
  return (
    <div><Link to={ '/admin/user/'+row._id }>{cell}</Link></div>
  );
}

function enumFormatter(cell, row, enumObject) {
  return enumObject[cell];
}
function createCustomButtonGroup(props){
    return (
        <Button
          bsStyle="primary"
          onClick={ addUserMode }>
          Добавить пользователя
        </Button>
    );
  }

    const options = {
      btnGroup: createCustomButtonGroup,
      noDataText: 'Нет данных'

      //afterSearch: afterSearch  // define a after search hook
    };  
  return (
      <BootstrapTable data={ userslist } 
      options={ options }
      search={ true }
      searchPlaceholder='Поиск...'
      >
        <TableHeaderColumn  dataField='fio' dataFormat={ usernameFormatter } className='react-bs-container-header' isKey dataSort headerAlign='center' width='300'>Пользователь</TableHeaderColumn>
        <TableHeaderColumn dataField='email' headerAlign='center'>Email</TableHeaderColumn>
        <TableHeaderColumn dataField='role' dataFormat={ enumFormatter } formatExtraData={roleType} headerAlign='center'>Роль</TableHeaderColumn>
        <TableHeaderColumn searchable={ false } dataField='testing_center' dataFormat={ centerFormatter } headerAlign='center'>Центры тестирования</TableHeaderColumn>
      </BootstrapTable>
  );
};

UsersListTable.propTypes = {
  userslist: PropTypes.array.isRequired,
};

export default UsersListTable;
