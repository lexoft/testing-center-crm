import React, { PropTypes } from 'react';
import UsersListAddUserForm from './UsersListAddUserForm';
import Message from '../../containers/Message';

const UsersListAddUser = ({ onEntrySave, centerlist}) => {
  return (
    <div>
      <h1>Добавить пользователя</h1>
      <Message/>
      <UsersListAddUserForm
        onEntrySave={onEntrySave} 
        centerlist={centerlist}/>
    </div>
  );
};

UsersListAddUser.propTypes = {
	centerlist: PropTypes.array.isRequired,
  	onEntrySave: PropTypes.func.isRequired
};
export default UsersListAddUser;
