import React, {  Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Link } from 'react-router';
import axios from 'axios';
import '../../css/react-bootstrap-table.css';


export default class TestSpeciesTable extends Component {
  constructor(props) {
    super(props);
    this.getNewId = this.getNewId.bind(this);
    this.onAfterInsertRow = this.onAfterInsertRow.bind(this);
    this.onAfterDeleteRow = this.onAfterDeleteRow.bind(this); 
    this.onAfterSaveCell = this.onAfterSaveCell.bind(this); 
    }
 

  nameValidator(value) {
  if (value.length==0) {
    return 'Введите наименование';
  }
  return true;
}

 createCustomModalHeader(onClose, onSave) {
    return (
      <div className='modal-header'>
        <h3>Добавить вид тестирования</h3>
      </div>
    );
  }

onAfterInsertRow(row) {
  const {addTestSpecies}=this.props;
  addTestSpecies({'name':row.name});
}  





  createCustomModalFooter = (onClose, onSave) => {
    return (
      <div className="modal-footer">
      <span>
      <button type="button" className="btn btn-warning" onClick={ onClose }>Отмена</button>
      <button type="button" className="btn btn-success" onClick={ onSave }>Добавить</button>
      </span>
      </div>
    );

  }
getNewId(){
  return('123');
}
onAfterDeleteRow(rowKeys) {
  const {deleteTestSpecies} = this.props;
  deleteTestSpecies({'keys':rowKeys});
}  
customConfirm(next, dropRowKeys) {
    alertify
      .okBtn("Удалить")
      .cancelBtn("Отмена")
      .confirm("Удалить отмеченные виды тестирования?", function (ev) {
          next();
    });  
}
onAfterSaveCell(row, cellName, cellValue) {
  const {editTestSpecies} = this.props;
  editTestSpecies({'id':row._id, 'name':row.name});
}
render(){
  const {testspecies:{ specieslist}, new_id}=this.props;
  const selectRowProp = {
    mode: 'checkbox',
    bgColor: '#f0efeb',
  }; 
  const options = {
      insertModalHeader: this.createCustomModalHeader,
      insertModalFooter: this.createCustomModalFooter,
      afterInsertRow: this.onAfterInsertRow,   // A hook for after insert rows
      afterDeleteRow: this.onAfterDeleteRow,
      handleConfirmDeleteRow: this.customConfirm,
      insertText: 'Добавить',
      deleteText: 'Удалить',
      noDataText: 'Нет данных'
  };
  const cellEditProp = {
    mode: 'click',
    blurToSave: true,
    afterSaveCell: this.onAfterSaveCell,
  };
  return (
    <BootstrapTable data={ specieslist }
      options={ options }
      selectRow={ selectRowProp }
      search={ true } 
      cellEdit={ cellEditProp }
      insertRow
      searchPlaceholder='Поиск...'
      deleteRow
      >
        <TableHeaderColumn dataField='_id' autoValue={ this.getNewId }  editable={ { type: 'textarea', validator: this.jobNameValidator } }  searchable={ false } isKey hidden>ID</TableHeaderColumn>
        <TableHeaderColumn dataField='name' editable={ { validator: this.nameValidator } } headerAlign='center'>Наименование</TableHeaderColumn>        
      </BootstrapTable>
  );
}
}

TestSpeciesTable.propTypes = {
  new_id: PropTypes.func.isRequired,
  deleteTestSpecies: PropTypes.func.isRequired,
  editTestSpecies: PropTypes.func.isRequired,
};
