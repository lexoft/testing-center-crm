import React, { Component, PropTypes } from 'react';


import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import ReactDOM from 'react-dom';
import {ControlLabel, Col, FormControl, FieldGroup, FormGroup, Button} from 'react-bootstrap';
import Select from 'react-select';
import '../../css/react-select.css';

export default class UsersListAddUserForm extends Component {
  constructor(props) {
    super(props);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.updateRoleValue = this.updateRoleValue.bind(this);
    this.updateCenterListValue = this.updateCenterListValue.bind(this);
    this.state={
      role:0,
      centerlist:''
    };
    }
  handleOnSubmit(event) {
    event.preventDefault();
    const {centerlist}=this.props;
    var new_centerlist={};
    centerlist.map(list => ( new_centerlist[list._id]=list.short_name));
    var centersSelect=this.state.centerlist.split(',');
    if(centersSelect[0]=="") centersSelect=[];
    var testing_center = centersSelect.map(function(option){
      return ({'id': option, 'name':new_centerlist[option]});
    });
    const role = this.state.role;
    const fio = ReactDOM.findDOMNode(this.refs.fio).value;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    const password = ReactDOM.findDOMNode(this.refs.password).value;
    const { onEntrySave } = this.props;
    onEntrySave({fio, email, password, testing_center, role});
  }
  componetnDidMount(){

  }
  updateRoleValue (newValue) {
    this.setState({
      role: newValue
    });
  }
  updateCenterListValue (newValue) {
    this.setState({
      centerlist: newValue
    });
  }

render(){
  const { centerlist } = this.props;
  return (
    <Col className="formadd" sm={6}>
      <form onSubmit={this.handleOnSubmit}>
          <FormGroup controlId="formControlsFio">
            <ControlLabel>ФИО:</ControlLabel>
              <FormControl type="text"
              required
                ref="fio"
                 />    
          </FormGroup>
          <FormGroup controlId="formControlsEmail">
            <ControlLabel>Email:</ControlLabel>
              <FormControl type="mail"
              required
                ref="email"
                 />    
          </FormGroup> 
          <FormGroup controlId="formControlsPassword"> 
            <ControlLabel>Пароль:</ControlLabel>
              <FormControl type="password"
              required
              ref="password"
              />    
          </FormGroup>   
          <FormGroup controlId="formControlscenter"> 
            <ControlLabel>Центры тестирования:</ControlLabel>
              <Select multi ref="centerlist" 
              clearAllText="Очистить"              
                options={centerlist} 
                labelKey="short_name" valueKey="_id"
                value={this.state.centerlist}
                onChange={this.updateCenterListValue}
                placeholder=''
              simpleValue searchable={false} clearable={true}
              />             
          </FormGroup>   
          <FormGroup controlId="formControlsRole"> 
            <ControlLabel>Роль:</ControlLabel>
              <Select ref="role" 
                options={[
                    {value: 0, label: 'Наблюдатель'},
                    {value: 1, label: 'Менеджер'},
                    {value: 2, label: 'Руководитель'},
                    {value: 3, label: 'Администратор системы'}
                  ]} 
                value={this.state.role}
                onChange={this.updateRoleValue}
              simpleValue searchable={false} clearable={false}
              /> 
          </FormGroup>
                              
      <Button  className="btn-primary" type="submit"
      >
       Добавить
      </Button>
      </form>
    </Col>
  );
}
};

UsersListAddUserForm.propTypes = {
  centerlist: PropTypes.array.isRequired,
  onEntrySave: PropTypes.func,
};
