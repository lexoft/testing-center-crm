import React, { PropTypes } from 'react';
import TestingCentersAddForm from './TestingCentersAddForm';

const TestingCentersAdd = ({ onEntrySave }) => {
  return (
    <div>
      <h1>Добавить центр тестирования</h1>
      <TestingCentersAddForm
        onEntrySave={onEntrySave} />
    </div>
  );
};

TestingCentersAdd.propTypes = {
  onEntrySave: PropTypes.func.isRequired
};
export default TestingCentersAdd;
