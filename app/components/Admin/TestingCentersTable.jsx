import React, { PropTypes } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Link } from 'react-router';
import {Button} from 'react-bootstrap';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import '../../css/react-bootstrap-table.css';

const TestingCentersTable = ({ centerlist, addTestingCenterMode }) => {
function short_nameFormatter(cell, row) {
  return (
    <div><Link to={ '/admin/testing_center/'+row._id }>{cell}</Link></div>
  );
}
function headFormatter(cell, row) {
  return (
    <div><div>{row.head}</div> <div>{row.position}</div> </div>
  );
}
function createCustomButtonGroup(props){
    return (
        <Button
          bsStyle="primary"
          onClick={ addTestingCenterMode }>
          Добавить центр тестирования
        </Button>
    );
  }
    const options = {
      btnGroup: createCustomButtonGroup,
      noDataText: 'Нет данных'
    };  
  return (
      <BootstrapTable data={ centerlist }
      options={ options }
      search={ true }
      searchPlaceholder='Поиск...'
      >
        <TableHeaderColumn dataField='short_name' dataFormat={ short_nameFormatter } isKey>Краткое наименование центра</TableHeaderColumn> 
        <TableHeaderColumn dataField='full_name' >Полное наименование центра</TableHeaderColumn>              
        <TableHeaderColumn dataField='threshold' width="80px">Порог</TableHeaderColumn>        
        <TableHeaderColumn dataField='adress' width="150px">Адресс</TableHeaderColumn>
        <TableHeaderColumn dataField='head' dataFormat={ headFormatter }>Руководитель</TableHeaderColumn>
        <TableHeaderColumn dataField='contact'>Контакты</TableHeaderColumn>
      </BootstrapTable>
  );
};

TestingCentersTable.propTypes = {
  centerlist: PropTypes.array.isRequired,
};

export default TestingCentersTable;
